//
// Created by alexey.nikitin on 05.03.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

@UIApplicationMain class AppPaidDelegate: AppBaseDelegate {
    override func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        super.application(application, didFinishLaunchingWithOptions: launchOptions)

        Flurry.startSession("C4J5K8269ZCZRFQM947R")
        AppsFlyerTracker.sharedTracker().appleAppID = "1089153216"

        return true
    }
}
