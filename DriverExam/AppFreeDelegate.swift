//
// Created by alexey.nikitin on 05.03.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import AdSupport
import Branch

@UIApplicationMain class AppFreeDelegate: AppBaseDelegate {
    private var signal: OneSignal!

    override func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        super.application(application, didFinishLaunchingWithOptions: launchOptions)

        initFlurry()
        initOneSignal(launchOptions)
        initAppsFlyer()
        initYandex()
        initBranch(launchOptions)

        return true
    }

    static func oneSignal() -> OneSignal! {
        let delegate = UIApplication.sharedApplication().delegate as! AppFreeDelegate
        return delegate.signal
    }

    func initFlurry() {
        Flurry.startSession("SQSXBPST3XPPNCQPZYWY")
    }

    func initOneSignal(launchOptions: [NSObject: AnyObject]?) {
        signal = OneSignal(launchOptions: launchOptions, appId: "c984ba50-0281-4222-b1dd-19555ed92b31", handleNotification: {
            (message, additionalData, isActive) in
            NSLog("\(message), \(additionalData), \(isActive)")
        })
        OneSignal.defaultClient().enableInAppAlertNotification(true)

        if Prefs.firstLaunch {
            signal.sendTag("showedPurchaseFormFromTraining", value: "no")
            Prefs.firstLaunch = false;
        }

        let type = Prefs.fullVersionBought ? "paid" : "free"
        signal.sendTag("type", value: type)
    }

    func initAppsFlyer() {
        AppsFlyerTracker.sharedTracker().appleAppID = "1082219340"
    }

    func initYandex() {
        YMMYandexMetrica.activateWithApiKey("4a3ae206-0e98-4845-8d81-439191b893f5")
        YMMYandexMetrica.setLoggingEnabled(true)
        YMMYandexMetrica.setReportCrashesEnabled(true)
    }
    
    func initBranch(launchOptions: [NSObject: AnyObject]?) {
        if Prefs.fullVersionBought {
            NSLog("Full version bought, do not init Branch")
            return
        }

//        Branch.getInstance().setDebug();

        let idfaString = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        NSLog("idfa: \(idfaString)")

        Branch.getInstance().setIdentity(idfaString, withCallback: {
            (params, error) in

            if error != nil {
                NSLog("Error: \(error)")
                return
            }

            self.initBranchSession(launchOptions)
        })
    }

    func initBranchSession(launchOptions: [NSObject: AnyObject]?) {
        Branch.getInstance().initSessionWithLaunchOptions(launchOptions, andRegisterDeepLinkHandler:{
            (params, error) in
            if error != nil {
                NSLog("Error: \(error)")
                return
            }

//            let isFirstSession = params["+is_first_session"] as? Bool
//            let clickedBranchLink = params["+clicked_branch_link"] as? Bool
//
            NSLog("Deep link data: \(params)")

            self.callBranchHistory()
        })
    }

    func callBranchHistory() {
        Branch.getInstance().getCreditHistoryWithCallback { (history: [AnyObject]!, error: NSError!) -> Void in
            NSLog("History: \(history) \(error)")

            if (error != nil) {
                return
            }

            var questionsAmount = 0
            var ticketsAmount = 0
            for item in history {
                let dict: NSDictionary = item as! NSDictionary

                let transaction: NSDictionary = dict["transaction"] as! NSDictionary;
                NSLog("Transaction: \(transaction)")

                let amount: Int = transaction["amount"] as! Int
                let bucketType = transaction["bucket"] as! String

                if bucketType == "questions" {
                    questionsAmount += amount
                }
                else if bucketType == "tickets" {
                    ticketsAmount += amount
                }
            }

            let oldQuestionsCount = Prefs.branchQuestions
            Prefs.branchQuestions = questionsAmount

            let oldTicketsCount = Prefs.branchTickets;
            Prefs.branchTickets = ticketsAmount

            if (oldQuestionsCount > 0 && oldQuestionsCount != questionsAmount) {
                Log.userGetAdditionalQuestions()
            }
        }
    }

    func application(application: UIApplication!, openURL url: NSURL!, sourceApplication: String!, annotation: AnyObject!) -> Bool {
        return Branch.getInstance().handleDeepLink(url)
    }

    func application(application: UIApplication!, continueUserActivity userActivity: NSUserActivity!, restorationHandler: ([AnyObject]!) -> ()) -> Bool {
        return Branch.getInstance().continueUserActivity(userActivity)
    }
}
