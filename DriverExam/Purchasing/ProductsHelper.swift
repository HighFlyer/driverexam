//
// Created by Alexey Nikitin on 09.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import StoreKit

/* Notification that is generated when a product is purchased. */
public let IdentifierFullVersionDefault = "DriverExamFullVersion"
public let IAPHelperProductPurchasedNotification = "IAPHelperProductPurchasedNotification"

/* Completion handler called when products are fetched. */
public typealias RequestProductsCompletionHandler = (success: Bool, products: [SKProduct]) -> ()

class ProductsHelper: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    var handler: (([SKProduct]?) -> ())?
    var restorePaymentHandler: ((Bool) -> ())?
    var paymentHandler: (() -> ())?
    var price: Int32 = 0

    func requestProduct(handler: ([SKProduct]?) -> ()) {
        NSLog("requestProduct")
        self.handler = handler

        let productsRequest = SKProductsRequest(productIdentifiers: [Prefs.priceID])
        productsRequest.delegate = self
        productsRequest.start()
    }

    @objc func productsRequest(request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {
        let products = response.products
        NSLog("productsRequest: products count: %d", products.count)

        for p in products {
            NSLog("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }

        self.handler?(products)
    }

    @objc func request(request: SKRequest, didFailWithError error: NSError) {
        NSLog("request: \(request), error: \(error)")
        self.handler?(nil)
    }

    func purchaseProduct(product: SKProduct, handler: () -> ()) {
        SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        NSLog("Buying \(product.productIdentifier)...")

        let payment = SKPayment(product: product)
        self.paymentHandler = handler
        SKPaymentQueue.defaultQueue().addPayment(payment)
    }

    func restorePayments(handler: (Bool) -> ()) {
        if !ProductsHelper.canMakePayments() {
            return;
        }

        self.restorePaymentHandler = handler;
        SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
    }

    func paymentQueue(queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        NSLog("Payment queue state changed: \(queue)")
        for transaction in transactions {
            NSLog("Transaction \(transaction) has new state: \(transaction.transactionState)")
            switch (transaction.transactionState) {
            case .Purchased:
                completeTransaction(transaction)
                break
            case .Failed:
                failedTransaction(transaction)
                break
            case .Restored:
                restoreTransaction(transaction)
                break
            case .Deferred:
                NSLog("State: Deferred")
                break
            case .Purchasing:
                NSLog("State: Purchasing")
                break
            }
        }
    }

    private func completeTransaction(transaction: SKPaymentTransaction) {
        NSLog("completeTransaction.... Original: \(transaction.originalTransaction)")
        provideContentForProductIdentifier(transaction.payment.productIdentifier)
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)

        if transaction.originalTransaction == nil {
            Log.purchased(price)
        }
        else {
            Log.repurchased(price)
        }

        if paymentHandler != nil {
            paymentHandler!()
            paymentHandler = nil
        }
    }

    private func restoreTransaction(transaction: SKPaymentTransaction!) {
        let productIdentifier = transaction.originalTransaction?.payment.productIdentifier
        NSLog("restoreTransaction... \(productIdentifier)")
        provideContentForProductIdentifier(productIdentifier)
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)

        Log.purchaseRecovered(productIdentifier)

        if paymentHandler != nil {
            paymentHandler!()
            paymentHandler = nil
        }
    }

    private func provideContentForProductIdentifier(productIdentifier: String!) {
        Prefs.fullVersionBought = true
        NSNotificationCenter.defaultCenter().postNotificationName(IAPHelperProductPurchasedNotification, object: productIdentifier)
    }

    private func failedTransaction(transaction: SKPaymentTransaction) {
        NSLog("failedTransaction...")
        if transaction.error?.code != SKErrorCode.PaymentCancelled.rawValue {
            NSLog("Transaction error: \(transaction.error?.localizedDescription)")
        }

        SKPaymentQueue.defaultQueue().finishTransaction(transaction)

        if paymentHandler != nil {
            paymentHandler!()
            paymentHandler = nil
        }
    }

    func performBuy(handler: () -> ()) {
        requestProduct({
            (products: [SKProduct]?) in

            if products?.count <= 0 {
                NSLog("No one product received")

                let alert = UIAlertView()
                alert.title = "Что-то пошло не так"
                alert.message = Utils.isInternetAvailable() ? "Что-то пошло не так. Попробуйте ещё раз" : "Похоже, нет подключения к Интернет."
                alert.addButtonWithTitle("Понял")
                alert.show()

                handler()
                return
            }

            let product = products!.first!
            self.price = product.price.intValue

            self.purchaseProduct(product, handler: handler)
        })
    }

    static func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }

    func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue) {
        NSLog("restore finished")
        restorePaymentHandler?(true)
    }

    func paymentQueue(queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: NSError) {
        NSLog("restore failed")
        restorePaymentHandler?(false)
    }
}
