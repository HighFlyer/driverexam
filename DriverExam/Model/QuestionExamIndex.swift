//
// Created by Alexey Nikitin on 05.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class QuestionExamIndex: NSObject, NSCoding {
    let questionId: Int32
    let additional: Bool
    let additionalIndex: Int32

    init(questionId: Int32, additionalIndex: Int32) {
        self.questionId = questionId
        self.additional = false
        self.additionalIndex = additionalIndex
        super.init()
    }

    init(questionId: Int32, additional: Bool, additionalIndex: Int32) {
        self.questionId = questionId
        self.additional = additional
        self.additionalIndex = additionalIndex
        super.init()
    }

    @objc required init(coder aDecoder: NSCoder) {
        questionId = aDecoder.decodeInt32ForKey("questionId")
        additional = aDecoder.decodeBoolForKey("additional")
        additionalIndex = aDecoder.decodeInt32ForKey("additionalIndex")
        super.init()
    }

    @objc func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInt32(questionId, forKey: "questionId")
        aCoder.encodeBool(additional, forKey: "additional")
        aCoder.encodeInt32(additionalIndex, forKey: "additionalIndex")
    }
}
