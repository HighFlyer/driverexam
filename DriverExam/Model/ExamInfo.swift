//
// Created by Alexey Nikitin on 07.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class ExamInfo: NSObject, NSCoding {
    var ticketNumber: Int32
    var successCount: Int32
    var attempts: Int32

    var questionIds: [QuestionExamIndex]
    private var incorrectAnswers: [IncorrectAnswer]
    var timeout: Bool = false
    var additionalAdded: Bool = false
    var incorrectAnswersCount: Int {
        return incorrectAnswers.count
    }
    var passed: Bool {
        var errorsCount = 0
        var additionalCount = 0

        for i in incorrectAnswers {
            if i.additional {
                additionalCount += 1
            }
            else {
                errorsCount += 1
            }
        }

        return errorsCount <= 2 && additionalCount == 0
    }

    init(ticketNumber: Int) {
        self.ticketNumber = Int32(ticketNumber)
        self.successCount = 0
        self.attempts = 0
        self.questionIds = [QuestionExamIndex]()
        self.incorrectAnswers = [IncorrectAnswer]()
        super.init()
    }

    @objc required init(coder aDecoder: NSCoder) {
        ticketNumber = aDecoder.decodeInt32ForKey("ticketNumber")
        successCount = aDecoder.decodeInt32ForKey("successCount")
        attempts = aDecoder.decodeInt32ForKey("attempts")
        questionIds = aDecoder.decodeObjectForKey("questionIds") as! [QuestionExamIndex]
        incorrectAnswers = aDecoder.decodeObjectForKey("incorrectAnswers") as! [IncorrectAnswer]
        additionalAdded = aDecoder.decodeBoolForKey("additionalAdded")
        timeout = aDecoder.decodeBoolForKey("timeout")
        super.init()
    }

    @objc func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInt32(ticketNumber, forKey: "ticketNumber")
        aCoder.encodeInt32(successCount, forKey: "successCount")
        aCoder.encodeInt32(attempts, forKey: "attempts")
        aCoder.encodeObject(questionIds, forKey: "questionIds")
        aCoder.encodeObject(incorrectAnswers, forKey: "incorrectAnswers")
        aCoder.encodeBool(additionalAdded, forKey: "additionalAdded")
        aCoder.encodeBool(timeout, forKey: "timeout")
    }

    func failed() -> Bool {
        return successCount <= 0 && attempts > 0
    }

    func success() -> Bool {
        return successCount > 0
    }

    func fetchQuestionIds() {
        questionIds = [QuestionExamIndex]()
        incorrectAnswers = [IncorrectAnswer]()

        do {
            let resultSet = try DBManager.instance.db.executeQuery(
                "SELECT DISTINCT _id FROM question WHERE ticket_number = ? ORDER BY question_number", Int(ticketNumber))

            var i = 0
            while resultSet.next() {
                questionIds.append(QuestionExamIndex(questionId: resultSet.intForColumnIndex(0), additionalIndex: Int32(i)))
                i += 1
            }

            resultSet.close()
        } catch {
            NSLog("Failed to init trainingQuestions")
        }
    }

    func nextQuestionIndex() -> QuestionExamIndex? {
        if questionIds.count <= 0 {
            return nil
        }

        return questionIds[0]
    }

    func incorrectAnswer(answerId: Int32) {
        let question = questionIds.removeFirst()
        incorrectAnswers.append(IncorrectAnswer(questionId: question.questionId, answerId: answerId, additional: question.additional))
        appendAdditional()
    }

    func correctAnswer() {
        questionIds.removeFirst()
        appendAdditional()
    }

    func appendAdditional() {
        if !hasMoreQuestions() && !additionalAdded {
            let count = incorrectAnswers.count
            if count > 0 && count <= 2 {
                additionalAdded = true

                do {
                    let resultSet = try DBManager.instance.db.executeQuery(
                        "SELECT DISTINCT _id FROM question ORDER BY RANDOM() LIMIT ?", 5 * count)

                    var i = 0
                    while resultSet.next() {
                        questionIds.append(QuestionExamIndex(questionId: resultSet.intForColumnIndex(0), additional: true, additionalIndex: 20 + Int32(i)))
                        i += 1
                    }

                    resultSet.close()
                } catch {
                    NSLog("Failed to init trainingQuestions")
                }
            }
        }
    }

    func hasMoreQuestions() -> Bool {
        return questionIds.count > 0
    }

    func incorrectQuestionId(index: Int) -> Int32 {
        return incorrectAnswers[index].questionId
    }

    func incorrectAnswerId(index: Int) -> Int32 {
        return incorrectAnswers[index].answerId
    }

    func updateForFinished() {
        if passed {
            successCount += 1
        }
    }

    func hasIncorrectAdditional() -> Bool {
        for i in incorrectAnswers {
            if i.additional {
                return true
            }
        }

        return false
    }
}
