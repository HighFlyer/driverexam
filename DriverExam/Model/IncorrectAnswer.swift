//
// Created by Alexey Nikitin on 05.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class IncorrectAnswer: NSObject, NSCoding {
    let questionId: Int32
    let answerId: Int32
    let additional: Bool

    init(questionId: Int32, answerId: Int32, additional: Bool) {
        self.questionId = questionId
        self.answerId = answerId
        self.additional = additional
        super.init()
    }

    @objc required init(coder aDecoder: NSCoder) {
        questionId = aDecoder.decodeInt32ForKey("questionId")
        answerId = aDecoder.decodeInt32ForKey("answerId")
        additional = aDecoder.decodeBoolForKey("additional")
        super.init()
    }

    @objc func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInt32(questionId, forKey: "questionId")
        aCoder.encodeInt32(answerId, forKey: "answerId")
        aCoder.encodeBool(additional, forKey: "additional")
    }
}
