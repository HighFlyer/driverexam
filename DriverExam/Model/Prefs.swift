//
// Created by Alexey Nikitin on 13.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class Prefs {
    static var shuffleAnswers: Bool {
        get {
            return safeBool("shuffleAnswers", true)
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "shuffleAnswers")
        }
    }

    static var fullVersionBought: Bool {
        get {
#if PAID_VERSION
            return true
#else
            return safeBool("DriverExamFullVersion", false)
#endif
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "DriverExamFullVersion")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    static var freeQuestionsCount: Int {
        get {
            return safeInt("freeQuestionsCount", FreeQuestionsDefault) + Prefs.branchQuestions
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "freeQuestionsCount")
        }
    }

    static var freeQuestionsCountInitial: Int {
        get {
            return safeInt("freeQuestionsCount", FreeQuestionsDefault)
        }
    }

    static var freeTicketsCount: Int {
        get {
            return safeInt("freeTicketsCount", FreeTicketsDefault) + Prefs.branchTickets
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "freeTicketsCount")
        }
    }

    static var freeTicketsCountInitial: Int {
        get {
            return safeInt("freeTicketsCount", FreeTicketsDefault)
        }
    }

    static var branchQuestions: Int {
        get {
            return safeInt("branchQuestions", 0)
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "branchQuestions")
        }
    }

    static var branchTickets: Int {
        get {
            return safeInt("branchTickets", 0)
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "branchTickets")
        }
    }

    static var questionsExperimentNumber: Int {
        get {
            return safeInt("questionsExperimentNumber", -1)
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "questionsExperimentNumber")
        }
    }

    static var priceExperimentNumber: Int {
        get {
            return safeInt("priceExperimentNumber", -1)
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "priceExperimentNumber")
        }
    }

    static var priceID: String {
        get {
            return IdentifierFullVersionDefault; // safeString("priceID", IdentifierFullVersionDefault)
        }
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "priceID")
        }
    }

    static var repeatedQuesionMarkShown: Bool {
        get {
            return safeBool("repeatedQuesionMarkShown", false)
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "repeatedQuesionMarkShown")
        }
    }

    static var firstLaunch: Bool {
        get {
            return safeBool("firstLaunch", true)
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "firstLaunch")
        }
    }

    static var appMarkPlaced: Bool {
        get {
            return safeBool("appMarkPlaced", false)
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setBool(newValue, forKey: "appMarkPlaced")
        }
    }

    static var buyFormShowCount: Int {
        get {
            return safeInt("buyFormShowCount", 0)
        }
        set (newValue) {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "buyFormShowCount")
        }
    }

    static var priceFormVariant: Int {
        get {
            return safeInt("priceFormVariant", 0)
        }
        set {
            NSUserDefaults.standardUserDefaults().setInteger(newValue, forKey: "priceFormVariant")
        }
    }

    static var lastInviteUrl: String {
        get {
            return safeString("lastInviteUrl", "")
        }
        set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "lastInviteUrl")
        }
    }

    static var freeQuestionsCountWithMark: Int {
        get {
            var freeQuestionsCount = Prefs.freeQuestionsCount
            if Prefs.appMarkPlaced {
                freeQuestionsCount += FreeQuestionsForMark
            }

            return freeQuestionsCount;
        }
    }

    static func shouldShowBuyPopup() -> Bool {
        if fullVersionBought {
            return false
        }

        return AppStateManager.instance.appState.totalUniqueTrainingQuestionsAnswered >= Int32(Prefs.freeQuestionsCountWithMark)
    }

    private static func safeBool(key: String, _ defVal: Bool) -> Bool {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) == nil ? defVal : NSUserDefaults.standardUserDefaults().boolForKey(key)
    }

    private static func safeInt(key: String, _ defVal: Int) -> Int {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) == nil ? defVal : NSUserDefaults.standardUserDefaults().integerForKey(key)
    }

    private static func safeString(key: String, _ defVal: String) -> String {
        return NSUserDefaults.standardUserDefaults().objectForKey(key) == nil ? defVal : NSUserDefaults.standardUserDefaults().stringForKey(key)!
    }

    static func shouldShowInvitePopup() ->  Bool {
#if CLONE_VERSION
        return false;
#endif
#if FREE_VERSION
        if Prefs.fullVersionBought {
            return false;
        }

        return Prefs.freeQuestionsCount < 800 && Prefs.freeTicketsCount < 40
#endif
    }
}
