//
// Created by Alexey Nikitin on 05.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

let ToughQuestions: [String] = [
            "17_3", "30_20", "9_11", "17_17", "39_6", "19_3",
            "26_13", "21_17", "20_9", "33_9", "23_20", "37_17",
            "34_17", "16_2", "11_10", "24_4", "9_4", "11_19",
            "10_8", "11_17", "20_20", "29_3", "11_20", "40_2",
            "31_17", "26_8", "36_12", "27_9",
            "8_3", "38_5", "35_12", "38_3", "27_13", "34_11",
            "10_2", "3_15", "7_6", "38_13", "26_20", "32_20"]

func fillTrainingQuestions(inout questions: [QuestionIndex]) {
    do {
        let resultSet = try DBManager.instance.db.executeQuery("SELECT DISTINCT _id FROM QUESTION ORDER BY RANDOM()")

        while resultSet.next() {
            questions.append(QuestionIndex(questionId: resultSet.intForColumnIndex(0), repeated: false))
        }

        resultSet.close()
    } catch {
        NSLog("Failed to init trainingQuestions")
    }
}

class AppState: NSObject, NSCoding {
    private var trainingQuestionsOriginalCount: Int32
    private var trainingQuestions: [QuestionIndex]
    private var examInfos: [ExamInfo]
    var totalTrainingSeconds: Int32
    var totalTrainingQuestionsAnswered: Int32
    var totalUniqueTrainingQuestionsAnswered: Int32
    var toughQuestionIndex: Int32

    override init() {
        NSLog("Init state with new data")
        trainingQuestions = [QuestionIndex]()

        fillTrainingQuestions(&trainingQuestions)
        trainingQuestionsOriginalCount = Int32(trainingQuestions.count)

        toughQuestionIndex = 0
        totalTrainingSeconds = 0
        totalTrainingQuestionsAnswered = 0
        totalUniqueTrainingQuestionsAnswered = 0
        examInfos = [ExamInfo]()

        super.init()
    }

    @objc required init(coder aDecoder: NSCoder) {
        trainingQuestions = aDecoder.decodeObjectForKey("trainingQuestions") as! [QuestionIndex]

        trainingQuestionsOriginalCount = aDecoder.decodeInt32ForKey("trainingQuestionsOriginalCount")
        totalTrainingQuestionsAnswered = aDecoder.decodeInt32ForKey("totalTrainingQuestionsAnswered")
        totalUniqueTrainingQuestionsAnswered = aDecoder.decodeInt32ForKey("totalUniqueTrainingQuestionsAnswered")
        totalTrainingSeconds = aDecoder.decodeInt32ForKey("totalTrainingSeconds")
        toughQuestionIndex = aDecoder.decodeInt32ForKey("toughQuestionIndex")
        examInfos = aDecoder.decodeObjectForKey("examsInfo") as! [ExamInfo]
    }

    @objc func encodeWithCoder(aCoder: NSCoder) {
        NSLog("AppState.encodeWithCoder")
        aCoder.encodeObject(trainingQuestions, forKey: "trainingQuestions")
        aCoder.encodeInt(trainingQuestionsOriginalCount, forKey: "trainingQuestionsOriginalCount")
        aCoder.encodeInt32(totalTrainingQuestionsAnswered, forKey: "totalTrainingQuestionsAnswered")
        aCoder.encodeInt32(totalUniqueTrainingQuestionsAnswered, forKey: "totalUniqueTrainingQuestionsAnswered")
        aCoder.encodeInt32(totalTrainingSeconds, forKey: "totalTrainingSeconds")
        aCoder.encodeInt32(toughQuestionIndex, forKey: "toughQuestionIndex")
        aCoder.encodeObject(examInfos, forKey: "examsInfo")
    }

    func passedQuestionPercentage() -> CGFloat {
        var uniqueQuestionIds = Set<Int32>()
        for q in trainingQuestions {
            uniqueQuestionIds.insert(q.questionId)
        }

        return CGFloat(100.0) * CGFloat(trainingQuestionsOriginalCount - uniqueQuestionIds.count) / CGFloat(trainingQuestionsOriginalCount)
    }

    func nextQuestionIndex() -> QuestionIndex? {
        if trainingQuestions.count <= 0 {
            return nil
        }

        return trainingQuestions[0]
    }

    func correctAnswer() {
        let index = trainingQuestions.removeAtIndex(0)
        totalTrainingQuestionsAnswered += 1

        if !index.repeated {
            totalUniqueTrainingQuestionsAnswered += 1
        }
    }

    func incorrectAnswer() {
        let incorrectQuestion = trainingQuestions[0]

        var i = 1;
        while i < trainingQuestions.count {
            let index = trainingQuestions[i]

            if index.questionId == incorrectQuestion.questionId {
                trainingQuestions.removeAtIndex(i)
                i -= 1
            }

            i += 1
        }

        let newIndex = QuestionIndex(questionId: incorrectQuestion.questionId, repeated: true)

        trainingQuestions.insert(newIndex, atIndex: min(4, trainingQuestions.count))

        if trainingQuestions.count >= 10 {
            trainingQuestions.insert(newIndex, atIndex: 10)
        }

        if trainingQuestions.count >= 20 {
            trainingQuestions.insert(newIndex, atIndex: 20)
        }

        if trainingQuestions.count >= 40 {
            trainingQuestions.insert(newIndex, atIndex: 40)
        }
    }

    func correctToughAnswer() {
        toughQuestionIndex += 1
    }

    func increaseTrainingDuration() {
        totalTrainingSeconds += 1
    }

    func examInfo(ticketNumber: Int) -> ExamInfo? {
        for info in examInfos {
            if info.ticketNumber == Int32(ticketNumber) {
                return info
            }
        }

        return nil
    }

    func successTicketsPercentage() -> Int {
        var count = 0

        for info in examInfos {
            if info.success() {
                count += 1
            }
        }

        return 100 * count / 40
    }

    func startAttempt(ticketNumber: Int) {
        var info = examInfo(ticketNumber)

        if info == nil {
            info = ExamInfo(ticketNumber: ticketNumber)
            examInfos.append(info!)
        }

        info!.fetchQuestionIds()
        info!.attempts += 1
    }

    func examsPassed() -> Int {
        var result = 0
        for i in 1...40 {
            let examInfo = self.examInfo(i)
            if examInfo != nil {
                if examInfo!.success() {
                    result += 1
                }
            }
        }

        return result
    }

    func hasMoreQuestions() -> Bool {
        return trainingQuestions.count > 0
    }

    func startNewTraining() {
        trainingQuestions = [QuestionIndex]()
        fillTrainingQuestions(&trainingQuestions)

        trainingQuestionsOriginalCount = Int32(trainingQuestions.count)
        totalTrainingSeconds = 0
        toughQuestionIndex = 0
    }

    func nextToughQuestionId() -> Int {
        let value: String = ToughQuestions[Int(toughQuestionIndex) % ToughQuestions.count]
        let chunks = value.characters.split{$0 == "_"}
        let ticketNumber = String(chunks[0])
        let questionNumber = String(chunks[1])

        do {
            let resultSet = try DBManager.instance.db.executeQuery(
                "SELECT _id FROM question WHERE ticket_number = ? AND question_number = ?", ticketNumber, questionNumber)

            if resultSet.next() {
                let result = resultSet.intForColumnIndex(0)
                resultSet.close()
                return Int(result)
            }

            resultSet.close()
        } catch {
            NSLog("Failed to init trainingQuestions")
        }

        return 0
    }
}
