//
// Created by Alexey Nikitin on 05.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class QuestionIndex: NSObject, NSCoding {
    let questionId: Int32
    let repeated: Bool

    init(questionId: Int32, repeated: Bool) {
        self.questionId = questionId
        self.repeated = repeated
        super.init()
    }

    @objc required init(coder aDecoder: NSCoder) {
        questionId = aDecoder.decodeInt32ForKey("questionId")
        repeated = aDecoder.decodeBoolForKey("repeated")
        super.init()
    }

    @objc func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeInt32(questionId, forKey: "questionId")
        aCoder.encodeBool(repeated, forKey: "repeated")
    }
}
