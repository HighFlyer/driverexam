//
// Created by Alexey Nikitin on 05.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class AppStateManager {
    static let instance = AppStateManager()

    var appState: AppState!
    private let appStateURL: NSURL

    init() {
        let documentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
        appStateURL = documentsDirectory.URLByAppendingPathComponent("AppState")!

        let appStateLocal = NSKeyedUnarchiver.unarchiveObjectWithFile(appStateURL.path!) as? AppState
        appState = appStateLocal != nil ? appStateLocal : AppState()
    }

    func resetState() {
        appState = AppState()
        save()
    }

    func save() {
        NSLog("Save AppState")
        NSKeyedArchiver.archiveRootObject(appState!, toFile: appStateURL.path!)
    }
}
