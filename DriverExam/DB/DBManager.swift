//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class DBManager {
    static let instance = DBManager()
    let db: FMDatabase;

    init() {
        let path = NSBundle.mainBundle().pathForResource("dr", ofType: "db")

        db = FMDatabase(path: path)
        db.open()
    }
}
