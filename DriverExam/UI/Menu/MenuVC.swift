//
//  MenuVC.swift
//  DriverExam
//
//  Created by Alexey Nikitin on 23.01.16.
//  Copyright © 2016 AppFlow. All rights reserved.
//

import UIKit
import StoreKit

class MenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var topBar: TopBarView!
    @IBOutlet weak var banner: UIView!
    @IBOutlet weak var buyButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        table.backgroundColor = UIColor.clearColor()
        let footer = UIView(frame: CGRectMake(0, 0, 0, 0))
        table.tableFooterView = footer
        table.contentInset = UIEdgeInsetsMake(0, 0, 56, 0)

        topBar.setTitle(nil)
        topBar.setInfoButtonImage("settings", active: "settings-active")
        topBar.backButton.hidden = true
        topBar.infoHandler = {
            self.performSegueWithIdentifier("settings", sender: self);
        }

        buyButton.backgroundColor = NegativeButtonBackgroundColor
        purchaseUpdated()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(purchaseUpdated), name: IAPHelperProductPurchasedNotification, object: nil)
    }

    func purchaseUpdated() {
        banner.hidden = Prefs.fullVersionBought
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func inviteButtonClicked() {
#if FREE_VERSION
        InvitePopup(frame: self.view.bounds, viewController: self).show("Menu")
#endif
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let header = tableView.dequeueReusableCellWithIdentifier("Header", forIndexPath: indexPath) as! MenuHeaderCell;

            let appState = AppStateManager.instance.appState
            header.ticketsProgress = appState.successTicketsPercentage();
            header.questionsProgress = Int(appState.passedQuestionPercentage());

            header.backgroundColor = UIColor.clearColor();
            header.backgroundView = UIView()
            header.selectedBackgroundView = UIView();
            header.inviteButton.addTarget(self, action: #selector(inviteButtonClicked), forControlEvents: .TouchUpInside)
            
            return header
        }
        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("General", forIndexPath: indexPath) as! MenuGeneralCell;

            switch indexPath.row {
                case 1:
                    cell.title.text = "Режим тренировки"
                    cell.descriptionLabel.text = "800 вопросов нон-стоп"
                    cell.indicator.backgroundColor = UIColor(red: 41 / 255.0, green: 200 / 255.0, blue: 136 / 255.0, alpha: 1)
                case 2:
                    cell.title.text = "Экзамен"
                    cell.descriptionLabel.text = "Решение билетов за 20 минут"
                    cell.indicator.backgroundColor = UIColor(red: 250 / 255.0, green: 80 / 255.0, blue: 79 / 255.0, alpha: 1)
                case 3:
                    cell.title.text = "Самые сложные"
                    cell.descriptionLabel.text = "Подборка незапоминаемых вопросов"
                    cell.indicator.backgroundColor = UIColor(red: 254 / 255.0, green: 215 / 255.0, blue: 60 / 255.0, alpha: 1)
                default:
                    NSLog("Ups")
            }

            return cell;
        }
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row > 0 {
            return 72
        }

        return Prefs.shouldShowInvitePopup() ? 357 : 297;
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBarHidden = true
        self.table.reloadData()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

//        self.navigationController?.navigationBarHidden = false
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        var action: String? = nil
        switch indexPath.row {
        case 1:
            action = "Training"
            self.performSegueWithIdentifier("ShowQuestion", sender:self)
        case 2:
            action = "Exam"
            self.performSegueWithIdentifier("ChooseTicket", sender:self)
        case 3:
            action = "ToughQuestions"
            self.performSegueWithIdentifier("ToughQuestions", sender:self)
        default:
            NSLog("Ups")
        }

        if action != nil {
            Log.mainMenuClick(action)
        }
    }

    @IBAction func buyClicked() {
        Log.mainMenuClick("Buy")
        PopupView.showBuyPopup(self.view, viewController: self, source: "Menu")
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if segue.identifier == "ToughQuestions" {
            let vc = segue.destinationViewController as! QuestionVC

            vc.isTough = true
            vc.topBarColor = ToolbarQuestionBackgroundColor

            self.navigationController?.popViewControllerAnimated(false)
        }
        else if segue.identifier == "settings" {
            Log.mainMenuClick("Settings")
        }
        else if segue.identifier == "ShowQuestion" {
            let vc = segue.destinationViewController as! QuestionVC

            vc.topBarColor = ToolbarQuestionBackgroundColor

            self.navigationController?.popViewControllerAnimated(false)
        }
    }
}
