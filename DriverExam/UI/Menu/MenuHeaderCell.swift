//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class MenuHeaderCell: UIKit.UITableViewCell {
    var ticketsProgress: Int {
        get { return 0 }
        set(newValue) {
            tickersView.percents = newValue;
        }
    }
    var questionsProgress: Int {
        get { return 0 }
        set(newValue) {
            questionsView.percents = newValue;
        }
    }

    @IBOutlet weak var tickersView: ProgressView!
    @IBOutlet weak var questionsView: ProgressView!
    @IBOutlet weak var inviteLabel: UILabel!
    @IBOutlet weak var inviteButton: UIButton!
    @IBOutlet weak var separator: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        tickersView.progressColor = UIColor(red: 252.0 / 255, green: 101.0 / 255, blue: 108.0 / 255, alpha: 1)
        questionsView.progressColor = UIColor(red: 41.0 / 255, green: 200.0 / 255, blue: 136.0 / 255, alpha: 1)
    }
}
