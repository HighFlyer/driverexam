//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class MenuGeneralCell: UIKit.UITableViewCell {
    @IBOutlet weak var indicator: UIView!;
    @IBOutlet weak var title: UILabel!;
    @IBOutlet weak var descriptionLabel: UILabel!;
}
