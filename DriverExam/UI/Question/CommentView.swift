//
// Created by Alexey Nikitin on 30.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class CommentView: UIKit.UIView {
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var questionLabel: ToughAttributedLabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var positiveButton: UIButton!

    override init(frame: CGRect) {
        super.init(frame: frame)

        let view = NSBundle.mainBundle().loadNibNamed("Comment", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = CGRectMake(5, 20, frame.size.width - 10, frame.size.height - 25)
        self.backgroundColor = UIColor(white: 0.1, alpha: 0.7)

        self.positiveButton?.backgroundColor = NegativeButtonBackgroundColor
        self.positiveButton?.setTitleColor(NegativeButtonTextColor, forState: .Normal)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func configureWithImage(image: UIImage?, question: String!, comment: String?) {
        questionImage.image = image;
        questionLabel.setFormattedText(question)
        commentLabel.text = comment

        questionLabel.sizeToFit()
        commentLabel.sizeToFit()
    }

    @IBAction func onClicked(sender: UIView) {
        self.removeFromSuperview()
    }
}
