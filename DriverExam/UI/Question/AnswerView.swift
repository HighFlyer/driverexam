//
// Created by Alexey Nikitin on 28.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class AnswerView: UIKit.UIView {
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var textLabel: ToughAttributedLabel!
    @IBOutlet weak var separator: UIView!

    var contentView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)

        contentView = NSBundle.mainBundle().loadNibNamed("Answer", owner: self, options: nil)![0] as! UIView;
        self.addSubview(contentView)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configureWithAnswer(text: String, y: CGFloat) {
        textLabel.setFormattedText(text)

        let constraintRect = CGSize(width: self.frame.size.width - 2 * textLabel.frame.origin.x, height: CGFloat.max)
        let r: CGRect = textLabel.attributedText!.boundingRectWithSize(
            constraintRect,
                    options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                    context: nil)

        let resultHeight = max(50, r.size.height + 20)

        self.frame = CGRectMake(0, y, self.frame.size.width, resultHeight)
        self.contentView.frame = CGRectMake(0, 0, self.frame.size.width, resultHeight)

        let textLabelY = (resultHeight - r.size.height) / 2
        textLabel.frame = CGRectMake(textLabel.frame.origin.x, textLabelY, r.size.width, r.size.height)

        centerButton.frame = CGRectMake(0, 0, self.frame.size.width, resultHeight)

        separator.frame = CGRectMake(0, resultHeight - 1, self.frame.size.width, 1)
    }

    func updateForIncorrect() {
        self.textLabel.textColor = UIColor.whiteColor()
        self.centerButton.backgroundColor = AnswerIncorrectColor
    }

    func updateForCorrect() {
        self.textLabel.textColor = UIColor.whiteColor()
        self.centerButton.backgroundColor = AnswerCorrectColor
    }

    func updateForNeutral() {
        self.textLabel.textColor = UIColor.whiteColor()
        self.centerButton.backgroundColor = AnswerNeutralColor
    }

    func updateForDontKnow() {
        self.textLabel.textColor = UIColor.whiteColor()
        self.centerButton.backgroundColor = UIColor(red: 255 / 255.0, green: 177 / 255.0, blue: 49 / 255.0, alpha: 1)
    }

    func updateForInitial() {
        self.textLabel.textColor = UIColor.blackColor()
        self.centerButton.backgroundColor = UIColor.clearColor()
    }
}
