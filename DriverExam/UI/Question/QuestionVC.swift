//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit.UIViewController

class QuestionVC: UIViewController {
    @IBOutlet weak var ticketLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var questionText: ToughAttributedLabel!
    @IBOutlet weak var dataContainer: UIView!
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var topBar: TopBarView!
    @IBOutlet weak var answersContainer: UIView!
    @IBOutlet weak var timerIcon: UIImageView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var percentsLabel: UILabel!
    @IBOutlet weak var questionRepeatedMark: UIView!

    var commentText: String?
    var alreadyAnswered: UIView?
    var timeoutView: UIView?
    var successView: UIView?
    var countDownTimer: NSTimer?
    var totalDurationTimer: NSTimer?
    var seconds: Int = 0
    var answersConstrains: NSLayoutConstraint?;
    var isExam: Bool = false;
    var isTough: Bool = false;
    var ticketNumber: Int = 0;
    var topBarColor: UIColor?
    var questionId: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        topBar.setTitle(isExam ? "Экзамен" : isTough ? "Сложные" : "Тренировка")
        topBar.setInfoButtonImage(isExam ? "i-red" : "i-green", active: nil)
        topBar.backHandler = {
            if !self.isExam {
                self.navigationController?.popViewControllerAnimated(true)
            }
            else {
                let alertController = UIAlertController(title: "Вы хотите выйти в самый разгар экзамена?",
                        message: "Билет будет не засчитан",
                        preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "Да", style: .Destructive, handler: {
                    (action: UIAlertAction!) in self.navigationController?.popViewControllerAnimated(true)
                }))
                alertController.addAction(UIAlertAction(title: "Нет", style: .Default, handler: nil))

                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
        topBar.infoHandler = {
            let popup = PopupView(frame: self.view.bounds, singleButton: true)
            if self.isExam {
                popup.configure("Проверьте, готовы ли вы к экзамену",
                        desc: "У вас есть 20 минут, чтобы решить 20 вопросов. Вы можете сделать не более 2 ошибок.\n\nЕсли вы допустили ошибку, к билету добавляются 5 вопросов на тему, в которой вы ошиблись. В дополнительных вопросах ошибаться нельзя.",
                        icon: nil)
            }
            else if self.isTough {
                popup.configure("Самые сложные",
                        desc: "Мы отобрали самые сложные вопросы, на которых отвечающие ошибаются чаще всего.",
                        icon: nil)
            }
            else {
                popup.configure("Режим тренировки",
                        desc: "В режиме тренировки показываются вопросы в такой последовательности, чтобы максимально быстро подготовиться к экзамену.\n\nВопросы, на которые вы ответили с ошибкой, мы повторяем несколько раз, чтобы закрепить полученные знания.",
                        icon: nil)
            }
            popup.show({popup.hide()}, {})
        }

        answersConstrains = NSLayoutConstraint(item: answersContainer, attribute: .Height,
                relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
        answersContainer.addConstraint(answersConstrains!)

        topBar.backgroundColor = topBarColor
        if (isExam) {
            seconds = ExamTimeout
            percentsLabel.hidden = true
        }

        nextQuestion()
    }

    func onAppBackground() {
        if !isExam && !isTough {
            totalDurationTimer?.invalidate()
            totalDurationTimer = nil
        }
        if isExam {
            countDownTimer?.invalidate()
            countDownTimer = nil
        }
    }

    func onAppForeground() {
        if !isExam && !isTough {
            totalDurationTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(QuestionVC.onTotalTimerTick), userInfo: nil, repeats: true)
        }
        else if isExam {
            if (seconds > 0) {
                countDownTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(QuestionVC.onTimerTick), userInfo: nil, repeats: true)
            }
        }
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBarHidden = true

        onAppForeground()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QuestionVC.onAppBackground), name:UIApplicationDidEnterBackgroundNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(QuestionVC.onAppForeground), name:UIApplicationWillEnterForegroundNotification, object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

//        self.navigationController?.navigationBarHidden = false

        onAppBackground()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    func nextQuestion() {
        let appState = AppStateManager.instance.appState

        var repeated = false
        var additional = false
        var additionalIndex = Int32(0)
        if isExam {
            let examInfo = appState.examInfo(ticketNumber)

            let questionIndex = examInfo!.nextQuestionIndex()
            questionId = Int(questionIndex!.questionId)
            additional = questionIndex!.additional
            additionalIndex = questionIndex!.additionalIndex
        }
        else if isTough {
            questionId = appState.nextToughQuestionId()

            timerIcon.hidden = true
            timerLabel.hidden = true
            percentsLabel.hidden = true
        }
        else {
#if !PAID_VERSION
        #if FREE_VERSION
            let oneSignal = AppFreeDelegate.oneSignal()
        #endif
        #if CLONE_VERSION
            let oneSignal = AppDuplicateDelegate.oneSignal()
        #endif
            oneSignal.sendTag("numberOfUniqueAnsweredQuestions", value: String(appState.totalUniqueTrainingQuestionsAnswered))
#endif
            let questionIndex = appState.nextQuestionIndex()

            if questionIndex != nil {
                questionId = Int(questionIndex!.questionId)
                repeated = questionIndex!.repeated

                let showPercents = AppStateManager.instance.appState.totalTrainingQuestionsAnswered % 10 <= 7

                timerIcon.hidden = showPercents
                timerLabel.hidden = showPercents
                percentsLabel.hidden = !showPercents

                updateTimerAndPercents()
            }
            else {
                appState.startNewTraining()
                nextQuestion()
            }
        }

        for v in answersContainer.subviews {
            v.removeFromSuperview()
        }

        questionRepeatedMark.hidden = isExam || !repeated

        do {
            var result: FMResultSet = try DBManager.instance.db.executeQuery(
                    "SELECT * FROM question WHERE _id = ?", questionId)

            if result.next() {
                let text = result.stringForColumn("_text")
                let question = result.intForColumn("question_number")
                let ticket = result.intForColumn("ticket_number")

                commentText = result.stringForColumn("comment")

                if !isExam || !additional {
                    ticketLabel.text = "Билет \(ticket)"
                    questionLabel.text = "Вопрос \(question)"
                }
                else {
                    ticketLabel.text = "Билет \(ticketNumber)"
                    questionLabel.text = "Вопрос \(1 + additionalIndex)"
                }

                questionText.setFormattedText(text)

                let imageData = result.dataForColumn("image_data")
                let image = imageData != nil ? UIImage(data: imageData) : nil

                if image != nil {
                    questionImage.image = image
                } else {
                    questionImage.image = UIImage(named: "question_placeholder")
                }
            } else {
                NSLog(":O question not found \(questionId)")
            }

            result.close()

            let orderBy = repeated && Prefs.shuffleAnswers && !isExam ? "RANDOM()" : "_index"

            result = try DBManager.instance.db.executeQuery("SELECT * FROM ANSWER WHERE question_id = ? ORDER BY \(orderBy)", values: [questionId])

            var prevY = CGFloat(0.0)

            while result.next() {
                let id = result.intForColumn("_id")
                let text = result.stringForColumn("_text")
                let isCorrect = result.intForColumn("is_correct")

                let answer = AnswerView(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
                answersContainer.addSubview(answer)
                answer.configureWithAnswer(text, y: prevY)
                answer.centerButton.addTarget(self, action: #selector(QuestionVC.answerClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                answer.tag = Int(isCorrect > 0 ? id : -id)

                prevY += answer.frame.height
            }

            if !isExam {
                prevY += 20
                let dontKnow = AnswerView(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
                answersContainer.addSubview(dontKnow)
                dontKnow.configureWithAnswer("Не знаю", y: prevY)
                dontKnow.centerButton.addTarget(self, action: #selector(QuestionVC.dontKnowClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
                prevY += dontKnow.frame.height
            }

            answersConstrains!.constant = prevY

            result.close()
        } catch {
            NSLog("Error")
        }

        if !isExam && repeated && !Prefs.repeatedQuesionMarkShown {
            Prefs.repeatedQuesionMarkShown = true
            AppStateManager.instance.save()

            Utils.performDelayed(1, {
                self.showAlreadyAnswered()
            })
        }
    }

    func showAlreadyAnswered() {
        let popup = PopupView(frame: self.view.bounds, singleButton: true)
        popup.configure("Вы уже отвечали на этот вопрос и ошиблись",
                desc: "Мы показываем такие вопросы несколько раз, чтобы закрепить материал",
                icon: "car")
        popup.show({popup.hide()}, {})
    }

    func dontKnowClicked(sender: UIButton) {
        let appState = AppStateManager.instance.appState

        if Prefs.shouldShowBuyPopup() {
            showBuyPopup()

            return
        }

        self.disableAnswers()
        
        let answerView = sender.superview?.superview as! AnswerView
        answerView.updateForDontKnow()
        appState.incorrectAnswer()
        onIncorrectAnswer()

        Log.trainingQuestionAnswered("DontKnow", questionId: questionId)
    }

    func answerClicked(sender: UIButton) {
        let appState = AppStateManager.instance.appState

        if !isExam && Prefs.shouldShowBuyPopup() {
            showBuyPopup()

            return
        }

        let answerView = sender.superview?.superview as! AnswerView

        let isCorrect = answerView.tag > 0

        self.disableAnswers()

        if isExam {
            let examInfo = appState.examInfo(ticketNumber)!

            let questionIndex = examInfo.nextQuestionIndex()!
            let additionalIndex = questionIndex.additionalIndex

            Log.examQuestionAnswered(isCorrect ? "Yes" : "No", ticketNumber: ticketNumber, questionId: questionId,
                    sequenceNumber: additionalIndex)

            answerView.updateForNeutral()
            if !isCorrect {
                appState.examInfo(ticketNumber)!.incorrectAnswer(-Int32(answerView.tag))
            }
            else {
                appState.examInfo(ticketNumber)!.correctAnswer()
            }

            if examInfo.hasMoreQuestions() {
                Utils.performDelayed(0.3, {
                    self.nextQuestion()
                })
            }
            else {
                examInfo.updateForFinished()
                countDownTimer?.invalidate()
                countDownTimer = nil
                seconds = 0

                AppStateManager.instance.save()

                if examInfo.incorrectAnswersCount > 0 {
                    performSegueWithIdentifier("results", sender: self)
                }
                else {
                    Log.examPassed(ticketNumber, result: "Success")
                    Utils.performDelayed(0.3, {
                        self.successView = NSBundle.mainBundle().loadNibNamed("ExamSuccess", owner: nil, options: nil)![0] as? UIView
                        self.successView!.backgroundColor = ExamResultSuccessColor
                        self.successView!.frame = self.view.bounds
                        let close = self.successView!.viewWithTag(10) as! UIButton

                        close.addTarget(self, action: #selector(QuestionVC.successClose(_:)), forControlEvents: .TouchUpInside)
                        close.backgroundColor = ExamResultExcellentButtonColor

                        self.successView!.alpha = 0
                        self.view.addSubview(self.successView!)
                        UIView.animateWithDuration(0.2, animations: {
                            self.successView!.alpha = 1
                        })
                    })
                }
            }
        }
        else if isTough {
            if !isCorrect {
                answerView.updateForIncorrect()
                onIncorrectAnswer()
            }
            else {
                answerView.updateForCorrect()
                appState.correctToughAnswer()

                Utils.performDelayed(0.3, {
                    self.nextQuestion()
                })
            }
        } else {
            if !isCorrect {
                answerView.updateForIncorrect()
                appState.incorrectAnswer()
                onIncorrectAnswer()
            }
            else {
                answerView.updateForCorrect()
                appState.correctAnswer()

                if !appState.hasMoreQuestions() {
                    Log.trainingFinished()
                    appState.startNewTraining()
                    AppStateManager.instance.save()

                    PopupView.showLastQuestion(self.view, {
                        self.nextQuestion()
                    })
                }
                else {
                    Utils.performDelayed(0.3, {
                        self.nextQuestion()
                    })
                }
            }

            Log.trainingQuestionAnswered(isCorrect ? "Yes" : "No", questionId: questionId)
        }
    }

    @IBAction func alredyAnsweredClicked(sender: AnyObject) {
        showAlreadyAnswered()
    }

    func disableAnswers() {
        for v in answersContainer.subviews {
            v.userInteractionEnabled = false
        }
    }

    func enableAnswers() {
        for v in answersContainer.subviews {
            v.userInteractionEnabled = true
        }
    }

    func onIncorrectAnswer() {
        Utils.performDelayed(0.3, {
            self.enableAnswers()

            for answer in self.answersContainer.subviews {
                let answerView = answer as! AnswerView
                answerView.updateForInitial()
            }

            let commentView = CommentView(frame: self.view.bounds)

            commentView.configureWithImage(self.questionImage.image, question: self.questionText.text, comment: self.commentText)

            commentView.alpha = 0
            UIApplication.sharedApplication().keyWindow?.addSubview(commentView)

            UIView.animateWithDuration(0.2, animations: {
                commentView.alpha = 1
            })
        })
    }

    func onTimerTick() {
        seconds -= 1
        NSLog("tick: %d sec left", seconds)

        let hoursLeft = seconds / 3600
        let minutesLeft = seconds / 60
        let secondsLeft = seconds % 60

        if hoursLeft > 0 {
            timerLabel.text = "\(hoursLeft):\(minutesLeft.format(2)):\(secondsLeft.format(2))"
        }
        else {
            timerLabel.text = "\(minutesLeft):\(secondsLeft.format(2))"
        }

        if (seconds <= 0) {
            countDownTimer?.invalidate()
            countDownTimer = nil

            let appState = AppStateManager.instance.appState
            let examInfo = appState.examInfo(ticketNumber)!
            examInfo.timeout = true

            if examInfo.incorrectAnswersCount <= 0 {
                Log.examPassed(ticketNumber, result: "Timeout")
                timeoutView = NSBundle.mainBundle().loadNibNamed("TimeOut", owner: nil, options: nil)![0] as? UIView
                timeoutView!.frame = self.view.bounds
                let close = timeoutView!.viewWithTag(10) as! UIButton

                close.addTarget(self, action: #selector(QuestionVC.timeoutClose(_:)), forControlEvents: .TouchUpInside)

                let text = timeoutView!.viewWithTag(11) as! ToughAttributedLabel
                text.setFormattedText(text.text)

                self.view.addSubview(timeoutView!)
            }
            else {
                performSegueWithIdentifier("results", sender: self)
            }

            AppStateManager.instance.save()
        }
    }

    func timeoutClose(sender: UIButton) {
        timeoutView!.removeFromSuperview()
        timeoutView = nil

        self.navigationController?.popViewControllerAnimated(false)
    }

    func successClose(sender: UIButton) {
        successView!.removeFromSuperview()
        successView = nil

        self.navigationController?.popViewControllerAnimated(false)
    }

    func onTotalTimerTick() {
        NSLog("onTotalTimerTick \(AppStateManager.instance.appState.totalTrainingSeconds)")
        AppStateManager.instance.appState.increaseTrainingDuration()

        updateTimerAndPercents()
    }

    func updateTimerAndPercents() {
        if !timerLabel.hidden {
            let seconds = AppStateManager.instance.appState.totalTrainingSeconds

            let minutesLeft = seconds / 60
            let secondsLeft = seconds % 60

            timerLabel.text = "\(minutesLeft):\(secondsLeft.format(2))"
        }
        else {
            let percents = AppStateManager.instance.appState.passedQuestionPercentage()
            percentsLabel.text = String(format: "%0.1f%%", percents)
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if segue.identifier == "results" {
            let vc = segue.destinationViewController as! ExamResultVC

            vc.ticketNumber = ticketNumber

            self.navigationController?.popViewControllerAnimated(false)
        }
    }

    private func showBuyPopup() {
        PopupView.showBuyPopup(self.view, viewController: self, source: isTough ? "Tough" : "Training")
    }
}
