//
// Created by Alexey Nikitin on 23.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

class PopupView: UIKit.UIView {
    @IBOutlet weak var titleLabel: ToughAttributedLabel?
    @IBOutlet weak var descriptionLabel: ToughAttributedLabel?
    @IBOutlet weak var popupIcon: UIImageView?
    @IBOutlet weak var positiveButton: UIButton?
    @IBOutlet weak var negativeButton: UIButton?
    @IBOutlet weak var progressView: UIView?
    @IBOutlet weak var recoverPayments: UILabel?
    @IBOutlet weak var backgroundImage: UIImageView?

    var successHandler: (() -> ())?
    var cancelHandler: (() -> ())?

    init(frame: CGRect, singleButton: Bool) {
        super.init(frame: frame)

        let view = NSBundle.mainBundle().loadNibNamed(singleButton ? "PopupViewSingle" : "PopupView", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.backgroundColor = PopupBackgroundColor

        view.frame = CGRectMake(5, 20, frame.size.width - 10, frame.size.height - 25)
        self.backgroundColor = UIColor(white: 0.1, alpha: 0.7)

        self.positiveButton?.backgroundColor = PositiveButtonBackgroundColor
        self.positiveButton?.setTitleColor(PositiveButtonTextColor, forState: .Normal)
        self.negativeButton?.backgroundColor = NegativeButtonBackgroundColor
        self.negativeButton?.setTitleColor(NegativeButtonTextColor, forState: .Normal)
    }

    init(frame: CGRect, buy: Bool) {
        super.init(frame: frame)

        let view = NSBundle.mainBundle().loadNibNamed("PopupViewBuy", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.backgroundColor = PopupBackgroundColor

        view.frame = CGRectMake(5, 20, frame.size.width - 10, frame.size.height - 25)
        self.backgroundColor = UIColor(white: 0.1, alpha: 0.7)

        if self.recoverPayments != nil {
            let attributeString = NSMutableAttributedString(string: self.recoverPayments!.text!)
            attributeString.addAttribute(NSUnderlineStyleAttributeName, value: 1, range: NSMakeRange(0, attributeString.length))

            self.recoverPayments!.attributedText = attributeString
        }

        self.positiveButton?.backgroundColor = PositiveButtonBackgroundColor
        self.positiveButton?.setTitleColor(PositiveButtonTextColor, forState: .Normal)
        self.negativeButton?.backgroundColor = NegativeButtonBackgroundColor
        self.negativeButton?.setTitleColor(NegativeButtonTextColor, forState: .Normal)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func configure(title: String, desc: String, icon: String?) {
        titleLabel!.setFormattedText(title)
        descriptionLabel!.setFormattedText(desc)
        if icon != nil {
            popupIcon!.image = UIImage(named: icon!)
            self.addConstraint(NSLayoutConstraint(item: titleLabel!, attribute: .Top, relatedBy: .Equal, toItem: popupIcon,
                    attribute: .Bottom, multiplier: 1, constant: icon == "buy_hidden" ? 105 : icon == "buy" ? 30 : 30))
        }
        else {
            popupIcon!.hidden = true
            self.addConstraint(NSLayoutConstraint(item: titleLabel!, attribute: .Top, relatedBy: .Equal, toItem: self.subviews[0],
                    attribute: .Top, multiplier: 1, constant: 35))
        }
    }

    @IBAction func positiveClicked() {
        self.showProgress()
        successHandler?()
    }

    func showProgress() {
        progressView?.hidden = false
        progressView?.alpha = 0
        UIView.animateWithDuration(0.3, animations: {
            self.progressView?.alpha = 1
        })
    }

    func hideProgress() {
        UIView.animateWithDuration(0.3, animations: {
            self.progressView?.alpha = 0
        })
    }

    @IBAction func negativeClicked() {
        cancelHandler?()
        hide()
    }

    func hide() {
        UIView.animateWithDuration(0.2, animations: {
            self.alpha = 0
        }, completion: {
            (value: Bool) in self.removeFromSuperview()
        })
    }

    func show(successHandler: () -> (), _ cancelHandler: () -> ()) {
        self.successHandler = successHandler
        self.cancelHandler = cancelHandler

        UIApplication.sharedApplication().keyWindow?.addSubview(self)
        self.alpha = 0
        UIView.animateWithDuration(0.2, animations: {
            self.alpha = 1
        })
    }

    private func configureForBuy() {
        positiveButton?.setTitle(ProductsHelper.canMakePayments() ? "Купить" : "Недоступно", forState: .Normal)
        negativeButton?.setTitle("Позже", forState: .Normal)

        var iconName = "buy"
        let variant = Prefs.priceFormVariant;
        if variant == 2 {
            self.backgroundImage?.image = UIImage(named: "buy_women")
            self.popupIcon?.hidden = true
            iconName = "buy_hidden"
        }
        else if variant == 3 {
            self.backgroundImage?.image = UIImage(named: "buy_man")
            self.popupIcon?.hidden = true
            iconName = "buy_hidden"
        }

        configure("НЕ ДЛЯ СЛАБАКОВ!",
                desc: PopupView.buyDescription("299"),
                icon: iconName)

        AppBaseDelegate.productsHelper().requestProduct({
            (products: [SKProduct]?) in
            if products?.count > 0 {
                let product = products![0];

                self.descriptionLabel?.setFormattedText(PopupView.buyDescription("\(product.price)"))
            }
        })
    }

    static func buyDescription(price: String) -> String {
        return "У нас тоже есть дети, и они хотят кушать. Приложение стоит всего \(price) рублей - это дешевле, чем обед в McDonald's."
    }

    static func showBuyPopup(view: UIView, viewController: UIViewController, source: String!) {
        let popup = PopupView(frame: view.bounds, buy: true)
        popup.configureForBuy()

        Prefs.buyFormShowCount += 1

        let successHandler = ProductsHelper.canMakePayments() ? {
            Log.purshaseFormClicked(source, "Buy")
            AppBaseDelegate.productsHelper().performBuy({popup.hide()})
        } :
        {
            Log.purshaseFormClicked(source, "Buy")
            popup.hide()
        }
#if FREE_VERSION
        let cancelHandler =
            source == "Training" || source == "Tough" ? {
            Log.purshaseFormClicked(source, "Later")
            if !Prefs.appMarkPlaced {
                self.showAppStorePopup(view)
            }
                
            else {
                InvitePopup(frame: view.bounds, viewController: viewController).show(source)
            }
        } : {
            Log.purshaseFormClicked(source, "Later")
            InvitePopup(frame: view.bounds, viewController: viewController).show(source)
        }
#endif
#if CLONE_VERSION
        let cancelHandler =
        {
            Log.purshaseFormClicked(source, "Later")
        }
#endif
        Log.showBuyForm(source)
        popup.show(successHandler, cancelHandler)

#if FREE_VERSION
        if source == "Training" {
            AppFreeDelegate.oneSignal().sendTag("showedPurchaseFormFromTraining", value: "yes")
        }
#endif
    }

    static func showLastQuestion(view: UIView, _ action: () -> ()) {
        let popup = PopupView(frame: view.bounds, singleButton: true)

        popup.positiveButton?.setTitle("Начать заново!", forState: .Normal)

        popup.configure("Поздравляем, вы готовы к экзамену!",
                desc: "Вы прошли тренировку! Самое время ответить на билеты в экзамене.",
                icon: nil)

        popup.show({
            popup.hide()
            action()
        }, {})
    }

    static func showAppStorePopup(view: UIView) {
        let popup = PopupView(frame: view.bounds, singleButton: false)

        popup.positiveButton?.setTitle("Конечно!", forState: .Normal)
        popup.negativeButton?.setTitle("Нет", forState: .Normal)

        popup.configure("Хотите продлить тренировку на 100 вопросов?",
                desc: "Оцените приложение в App Store и получите 100 вопросов для тренировки бесплатно.",
                icon: "appstore")

        popup.show({
            /* let iTunesLink = "https://itunes.apple.com/ru/app/ekzamen-pdd-2016-bilety-ekzamen/id1082219340?mt=8" */
#if PAID_VERSION
            let iTunesLink = ""
#endif
#if FREE_VERSION
            let iTunesLink = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1082219340"
#endif
#if CLONE_VERSION
            let iTunesLink = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=670312489"
#endif
            UIApplication.sharedApplication().openURL(NSURL(string: iTunesLink)!)

            Prefs.appMarkPlaced = true

            popup.hide()

            Log.ratePopupClosed("Yes")
        }, {
            Log.ratePopupClosed("No")
        })
    }

    @IBAction func restorePayments() {
        NSLog("PopupView: restorePayments")
        self.showProgress()
        AppBaseDelegate.productsHelper().restorePayments({
            (success: Bool) in

            if success && Prefs.fullVersionBought {
                self.hide()

                let alertController = UIAlertController(title: "Всё хорошо",
                        message: "Спасибо, ваши покупки восстановлены",
                        preferredStyle: .Alert)
                alertController.addAction(UIAlertAction(title: "Ок!", style: .Default, handler: {
                    (action: UIAlertAction!) in
                    alertController.removeFromParentViewController()
                }))

                UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
            }
            else {
                self.hideProgress()
            }
        })
    }
}
