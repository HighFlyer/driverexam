//
// Created by Alexey Nikitin on 23.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class GradientView: UIView {
    @IBInspectable var topColor: UIColor?
    @IBInspectable var bottomColor: UIColor?

    var gradientLayer: CAGradientLayer?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.clearColor()

        gradientLayer = CAGradientLayer()
        gradientLayer!.colors = [topColor!.CGColor, bottomColor!.CGColor]
        gradientLayer!.locations = [0.0, 1.0]

        self.layer.addSublayer(gradientLayer!)
    }

    override func layoutSubviews() {
        gradientLayer!.frame = self.bounds
    }
}
