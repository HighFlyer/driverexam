//
// Created by Alexey Nikitin on 13.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class SettingsVC: UIKit.UIViewController, UITableViewDataSource, UITableViewDelegate/*, MFMailComposeViewControllerDelegate*/ {
    @IBOutlet weak var topBar: TopBarView!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        topBar.setTitle("Настройки")
        topBar.backHandler = {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        topBar.backgroundColor = ToolbarSettingsBackgroundColor

        tableView.sectionHeaderHeight = 54

        let footer = UIView(frame: CGRectMake(0, 0, 0, 0))
        tableView.tableFooterView = footer

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(SettingsVC.purchaseUpdated), name: IAPHelperProductPurchasedNotification, object: nil)
    }

    func purchaseUpdated()  {
        tableView.reloadData()
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            let buyButton = Prefs.fullVersionBought ? 0 : 1
            let inviteFriends = Prefs.shouldShowInvitePopup() ? 1 : 0
            return 2 + buyButton + inviteFriends
        }

        return 1
    }

    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = NSBundle.mainBundle().loadNibNamed("SettingsViews", owner: nil, options: nil)![0] as! UIView

        let title = view.viewWithTag(1) as! UILabel

        title.text = section == 0 ? "НАСТРОЙКА ВОПРОСОВ" : "ИНФОРМАЦИЯ О КОМПАНИИ"

        return view
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == 0 {
            let result = NSBundle.mainBundle().loadNibNamed("SettingsViews", owner: nil, options: nil)![1] as! UITableViewCell
            let _switch = result.viewWithTag(1) as! UISwitch
            _switch.onTintColor = ToolbarSettingsBackgroundColor

            _switch.on = Prefs.shuffleAnswers
            _switch.addTarget(self, action: #selector(SettingsVC.switchChanged(_:)), forControlEvents: .ValueChanged)

            return result
        }

        let result = NSBundle.mainBundle().loadNibNamed("SettingsViews", owner: nil, options: nil)![2] as! UITableViewCell
        let label = result.viewWithTag(1) as! ToughAttributedLabel

        if (indexPath.section == 0) {
            var text = ""

            if indexPath.row == 1 {
                text = "Очистить историю"
            }
            else if indexPath.row == 2 {
                text = ProductsHelper.canMakePayments() ? "Восстановить покупки" : "Восстановить покупки (недоступно)"
            }
            else if indexPath.row == 3{
                text = "Пригласить друга"
            }

            label.setFormattedText(text)

            if indexPath.row == 2 {
                result.userInteractionEnabled = !Prefs.fullVersionBought
                label.enabled = !Prefs.fullVersionBought
            }
        }
        else {
            let appVersionString = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String

            label.lineSpacingPt = 4
            label.setFormattedText("Версия \(appVersionString)\n" +
                         "not@badenough.mobi")
        }

        return result
    }

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0 {
            return 90
        }

        if indexPath.section == 1 {
            return 80
        }

        return 60
    }

    func switchChanged(_switch: UISwitch) {
        Prefs.shuffleAnswers = _switch.on
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        if indexPath.section != 0 {
/*
            let docDirectory: NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
            let logpath = docDirectory.stringByAppendingPathComponent("Logs.txt")

            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self

            //Set the subject and message of the email
            mailComposer.setSubject("Logs")
            mailComposer.setToRecipients(["aleksvas@gmail.com"])

            if let fileData = NSData(contentsOfFile: logpath) {
                mailComposer.addAttachmentData(fileData, mimeType: "text/plain", fileName: "logs.txt")
            }
            self.presentViewController(mailComposer, animated: true, completion: nil)

*/
            return
        }

        switch indexPath.row {
        case 1:
            let alertController = UIAlertController(title: "Подтвердите",
                    message: "История тренировок и экзаменов будет удалено. Вы начнете все сначала. Вы уверены?",
                    preferredStyle: .Alert)
            alertController.addAction(UIAlertAction(title: "Да", style: .Destructive, handler: {
                (action: UIAlertAction!) in AppStateManager.instance.resetState()
            }))
            alertController.addAction(UIAlertAction(title: "Нет", style: .Default, handler: nil))

            self.presentViewController(alertController, animated: true, completion: nil)
        case 2:
            AppBaseDelegate.productsHelper().restorePayments({(success: Bool) in})
        case 3:
#if FREE_VERSION
            InvitePopup(frame: self.view.bounds, viewController: self).show("Settings")
#endif
        default:
            NSLog("Ups")
        }
    }

/*
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
*/
}
