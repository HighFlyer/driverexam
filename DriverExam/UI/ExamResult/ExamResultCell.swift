//
// Created by Alexey Nikitin on 13.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class ExamResultCell: UITableViewCell {
    @IBOutlet weak var questionImage: UIImageView!
    @IBOutlet weak var question: ToughAttributedLabel!
    @IBOutlet weak var yourAnswer: ToughAttributedLabel!
    @IBOutlet weak var rightAnswer: ToughAttributedLabel!
    @IBOutlet weak var comment: ToughAttributedLabel!

    var constraint: NSLayoutConstraint?

    func updateImageConstraint() {
        if questionImage.hidden {
            if constraint == nil {
                constraint = NSLayoutConstraint(item: questionImage, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
                constraint!.priority = 1000
                questionImage.addConstraint(constraint!)
            }
        }
        else {
            if constraint != nil {
                questionImage.removeConstraint(constraint!)
                constraint = nil
            }
        }
    }
}
