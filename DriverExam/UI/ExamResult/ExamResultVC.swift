//
// Created by Alexey Nikitin on 13.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class ExamResultVC: UIKit.UIViewController, UITableViewDataSource, UITableViewDelegate {
    var ticketNumber: Int = 0
    var examInfo: ExamInfo?

    @IBOutlet weak var tableView: UIKit.UITableView!
    @IBOutlet weak var incorrectAnswersLabel: UILabel!
    @IBOutlet weak var resultImage: UIImageView!
    @IBOutlet weak var resultText: UILabel!
    @IBOutlet weak var topColoredView: UIView!
    @IBOutlet weak var okButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 56, 0)

        examInfo = AppStateManager.instance.appState.examInfo(ticketNumber)
        incorrectAnswersLabel.text = "\(examInfo!.incorrectAnswersCount)"

        okButton.backgroundColor = NegativeButtonBackgroundColor

        if examInfo!.passed && !examInfo!.timeout {
            Log.examPassed(ticketNumber, result: "Success")
            resultImage.image = UIImage(named: "flags")
            resultText.text = "Поздравляем! \nВы сдали экзамен"
            topColoredView.backgroundColor = ExamResultSuccessColor
            resultImage.backgroundColor = ExamResultSuccessColor
        }
        else {
            Log.examPassed(ticketNumber, result: examInfo!.timeout ?
                    "Timeout" :
                    examInfo!.hasIncorrectAdditional() ?
                        "FailedAdditional" :
                        "FailedMain"
            )
            resultImage.image = UIImage(named: "timeout")
            resultText.text =
                    examInfo!.timeout ?
                        "Жаль, но экзамен не сдан.\nВремя вышло!" :
                        examInfo!.hasIncorrectAdditional() ?
                            "Жаль, но экзамен не сдан.\nВы допустили ошибки в дополнительных вопросах." :
                            "Жаль, но экзамен не сдан.\nВы сделали слишком много ошибок"
            topColoredView.backgroundColor = ExamResultFailedColor
            resultImage.backgroundColor = ExamResultFailedColor
        }
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return examInfo!.incorrectAnswersCount
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("question") as! ExamResultCell

        updateForGeneralInfo(cell, indexPath.row)
        updateForYourAnswer(cell, indexPath.row)
        updateForCorrectAnswer(cell, indexPath.row)

        return cell
    }

    func updateForGeneralInfo(cell: ExamResultCell, _ index: Int) {
        let questionId = examInfo!.incorrectQuestionId(index)

        do {
            let result: FMResultSet = try DBManager.instance.db.executeQuery(
            "SELECT * FROM question WHERE _id = ?", NSNumber(int: questionId))

            if result.next() {
                let text = result.stringForColumn("_text")

                let commentText = result.stringForColumn("comment")

                let imageData = result.dataForColumn("image_data")
                let image = imageData != nil ? UIImage(data: imageData) : nil
                cell.questionImage.image = image
                cell.questionImage.hidden = image == nil
                cell.updateImageConstraint()

                cell.question.setFormattedText(text)
                cell.comment.setFormattedText(commentText)
            } else {
                NSLog(":O question not found \(questionId)")
            }

            result.close()
        } catch {
            NSLog("Something weng wrong")
        }
    }

    func updateForYourAnswer(cell: ExamResultCell, _ index: Int) {
        let answerId = examInfo!.incorrectAnswerId(index)

        do {
            let result = try DBManager.instance.db.executeQuery("SELECT * FROM ANSWER WHERE _id = ?", values: [Int(answerId)])

            if result.next() {
                let text = result.stringForColumn("_text")
                cell.yourAnswer.setFormattedText("Ваш ответ: \(text)")
            }
            else {
                cell.yourAnswer.setFormattedText("Что-то пошло не так :(")
            }

            result.close()
        } catch {
            NSLog("Failed to fetch answer")
        }

    }

    func updateForCorrectAnswer(cell: ExamResultCell, _ index: Int) {
        let questionId = examInfo!.incorrectQuestionId(index)

        do {
            let result = try DBManager.instance.db.executeQuery("SELECT * FROM ANSWER WHERE question_id = ? AND is_correct > 0", values: [Int(questionId)])

            if result.next() {
                let text = result.stringForColumn("_text")
                cell.rightAnswer.setFormattedText("Правильный ответ: \(text)")
            }
            else {
                cell.rightAnswer.setFormattedText("Что-то пошло не так :(")
            }

            result.close()
        } catch {
            NSLog("Failed to fetch answer")
        }

    }

    @IBAction func goToMenu() {
        self.dismissViewControllerAnimated(false, completion: nil)
    }
}
