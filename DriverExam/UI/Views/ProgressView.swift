//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit.UIView

class ProgressView: UIView {
    let circleLineWidth = CGFloat(1.5);

    var digitsFont: UIFont? = UIFont(name: "GothamPro", size:23)
    var percentFont: UIFont? = UIFont(name: "GothamPro", size:12)
    var progressColor: UIColor?;

    var percents: Int! {
        didSet {
            setNeedsDisplay();
        }
    }

    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()

        CGContextSetLineWidth(context!, circleLineWidth)

        let angles = CGFloat(2 * M_PI * Double(percents) / 100);
        let startAngle = CGFloat(-M_PI_2);
        let endAngle = angles + startAngle;

        CGContextSetStrokeColorWithColor(context!, (progressColor?.CGColor)!)
        CGContextAddArc(context!, rect.midX, rect.midY, rect.width / 2 - circleLineWidth / 2, startAngle, endAngle, 0);
        CGContextStrokePath(context!)

        CGContextSetStrokeColorWithColor(context!, ProgressCircleColor.CGColor)
        CGContextAddArc(context!, rect.midX, rect.midY, rect.width / 2 - circleLineWidth / 2, endAngle, startAngle + CGFloat(M_PI) * 2, 0);
        CGContextStrokePath(context!)

        let digitsString: String = String(percents);
        let digitsRect: CGRect = digitsString.boundingRectWithSize(bounds.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: digitsFont!], context: nil);

        let percentString = "%"
        let percentsRect: CGRect = percentString.boundingRectWithSize(bounds.size, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: percentFont!], context: nil);

        let digitsX = (rect.width - digitsRect.width - percentsRect.width) / 2
        let digitsY = (rect.height - digitsRect.height) / 2

        let textFontAttributes = [
                NSFontAttributeName: digitsFont!,
                NSForegroundColorAttributeName: UIColor.whiteColor()
        ]

        digitsString.drawInRect(CGRectMake(digitsX, digitsY, digitsRect.width, digitsRect.height),
                withAttributes: textFontAttributes);

        let textFontAttributes2 = [
                NSFontAttributeName: percentFont!,
                NSForegroundColorAttributeName: UIColor.whiteColor()
        ]

        percentString.drawInRect(CGRectMake(digitsX + digitsRect.width, digitsY, percentsRect.width, percentsRect.height),
                withAttributes: textFontAttributes2);
    }
}
