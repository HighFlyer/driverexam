//
// Created by Alexey Nikitin on 24.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit.UIView

class ProgressLineView: UIView {
    let circleLineWidth = CGFloat(1.5);

    var progressColor: UIColor?;
    var progressCColor: UIColor?;

    var percents: Int! {
        didSet {
            setNeedsDisplay();
        }
    }

    var percentsCenter: Int! {
        didSet {
            setNeedsDisplay();
        }
    }

    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()

        CGContextSetLineWidth(context!, circleLineWidth)

        let xPoint = self.bounds.width * CGFloat(percents) / 100
        let xPointC = self.bounds.width * CGFloat(percentsCenter) / 100
        let centerY = self.bounds.height / 2

        CGContextSetStrokeColorWithColor(context!, (progressColor?.CGColor)!)
        CGContextMoveToPoint(context!, 0, centerY)
        CGContextAddLineToPoint(context!, xPointC, centerY)
        CGContextStrokePath(context!)

        CGContextSetStrokeColorWithColor(context!, (progressCColor?.CGColor)!)
        CGContextMoveToPoint(context!, xPointC, centerY)
        CGContextAddLineToPoint(context!, xPoint, centerY);
        CGContextStrokePath(context!)

        CGContextSetStrokeColorWithColor(context!, ProgressCircleColor.CGColor)
        CGContextMoveToPoint(context!, xPoint, centerY)
        CGContextAddLineToPoint(context!, self.bounds.width, centerY);
        CGContextStrokePath(context!)
    }
}
