//
// Created by alexey.nikitin on 11.04.16.
// Copyright (c) 2016 BadEnough. All rights reserved.
//

import Foundation

public let BLUE_TOOLBAR = UIColor(red: 0.0 / 255, green: 174.0 / 255, blue: 242.0 / 255, alpha: 1)
public let BLUE_BUTTON = UIColor(red: 1.0 / 255, green: 168.0 / 255, blue: 234.0 / 255, alpha: 1)
public let RED = UIColor(red: 252.0 / 255, green: 39.0 / 255, blue: 6.0 / 255, alpha: 1)
public let GREEN = UIColor(red: 0.0 / 255, green: 195.0 / 255, blue: 0.0 / 255, alpha: 1)

public let ProgressCircleColor = UIColor(red: 29.0 / 255, green: 138.0 / 255, blue: 194.0 / 255, alpha: 1)
public let ToolbarSettingsBackgroundColor = BLUE_TOOLBAR
public let ToolbarQuestionBackgroundColor = BLUE_TOOLBAR
public let ToolbarChooseTicketBackgroundColor = BLUE_TOOLBAR
public let PositiveButtonBackgroundColor = UIColor.whiteColor()
public let PositiveButtonTextColor = UIColor.blackColor()
public let NegativeButtonBackgroundColor = BLUE_BUTTON
public let NegativeButtonTextColor = UIColor.whiteColor()

public let ExamFailedColor = RED
public let ExamSuccessColor = GREEN

public let PopupBackgroundColor = BLUE_TOOLBAR

public let AnswerIncorrectColor = RED
public let AnswerCorrectColor = BLUE_BUTTON
public let AnswerNeutralColor = BLUE_BUTTON

public let ExamResultSuccessColor = BLUE_TOOLBAR
public let ExamResultFailedColor = BLUE_TOOLBAR
public let ExamResultExcellentButtonColor = BLUE_BUTTON

class ThemeClone {
}
