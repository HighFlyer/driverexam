//
// Created by alexey.nikitin on 11.04.16.
// Copyright (c) 2016 BadEnough. All rights reserved.
//

import Foundation

public let ProgressCircleColor = UIColor(red: 95.0 / 255, green: 69.0 / 255, blue: 95.0 / 255, alpha: 1)
public let ToolbarSettingsBackgroundColor = UIColor(red: 77.0 / 255, green: 167.0 / 255, blue: 155.0 / 255, alpha: 1)
public let ToolbarQuestionBackgroundColor = UIColor(red: 41.0 / 255, green: 200.0 / 255, blue: 136.0 / 255, alpha: 1)
public let ToolbarChooseTicketBackgroundColor = UIColor(red: 247.0 / 255, green: 82.0 / 255, blue: 84.0 / 255, alpha: 1)
public let PositiveButtonBackgroundColor = UIColor(red: 41.0 / 255, green: 200.0 / 255, blue: 136.0 / 255, alpha: 1)
public let PositiveButtonTextColor = UIColor(red: 255.0 / 255, green: 255.0 / 255, blue: 255.0 / 255, alpha: 1)
public let NegativeButtonBackgroundColor = UIColor(red: 120.0 / 255, green: 71.0 / 255, blue: 100.0 / 255, alpha: 1)
public let NegativeButtonTextColor = UIColor(red: 255.0 / 255, green: 255.0 / 255, blue: 255.0 / 255, alpha: 1)

public let ExamFailedColor = UIColor(red: 249 / 255.0, green: 102 / 255.0, blue: 111 / 255.0, alpha: 1)
public let ExamSuccessColor = UIColor(red: 120 / 255.0, green: 215 / 255.0, blue: 174 / 255.0, alpha: 1)

public let PopupBackgroundColor = UIColor(red: 113 / 255.0, green: 68 / 255.0, blue: 95 / 255.0, alpha: 1)

public let AnswerIncorrectColor = UIColor(red: 253 / 255.0, green: 115 / 255.0, blue: 76 / 255.0, alpha: 1)
public let AnswerCorrectColor = UIColor(red: 77 / 255.0, green: 167 / 255.0, blue: 155 / 255.0, alpha: 1)
public let AnswerNeutralColor = UIColor(red: 112 / 255.0, green: 69 / 255.0, blue: 95 / 255.0, alpha: 1)

public let ExamResultSuccessColor = UIColor(red: 41.0 / 255, green: 200.0 / 255, blue: 136.0 / 266, alpha: 1)
public let ExamResultFailedColor = UIColor(red: 246.0 / 255, green: 80.0 / 255, blue: 46.0 / 266, alpha: 1)
public let ExamResultExcellentButtonColor = UIColor(red: 116.0 / 255, green: 216.0 / 255, blue: 173.0 / 266, alpha: 1)

class ThemeBase {
}
