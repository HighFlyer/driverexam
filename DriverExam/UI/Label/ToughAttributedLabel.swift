//
// Created by Alexey Nikitin on 22.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class ToughAttributedLabel: UILabel {

    @IBInspectable var fontSize: CGFloat = 13.0
    @IBInspectable var fontFamily: String = "GothamPro"
    @IBInspectable var textAlign: String = "left"
    @IBInspectable var lineSpacingPt: CGFloat = 2

    override func awakeFromNib() {
        super.awakeFromNib()

        setFormattedText(self.text)
    }

    func setFormattedText(text: String!) {
        let attrString = NSMutableAttributedString(string: text)
        let font = UIFont(name: self.fontFamily, size: self.fontSize)
        self.font = font

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacingPt;
        paragraphStyle.alignment = "center" == textAlign ? .Center : .Left

        attrString.addAttribute(NSFontAttributeName, value: font!, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attrString.length))

        self.attributedText = attrString
    }
}