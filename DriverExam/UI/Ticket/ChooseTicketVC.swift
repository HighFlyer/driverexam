//
// Created by Alexey Nikitin on 30.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class ChooseTicketVC: UIKit.UIViewController {
    @IBOutlet weak var topBar: TopBarView!
    @IBOutlet weak var attemptsCount: UILabel!
    @IBOutlet weak var successCount: UILabel!
    @IBOutlet weak var attemptsCountLabel: UILabel!
    @IBOutlet weak var successCountLabel: UILabel!
    @IBOutlet weak var goToExam: UIButton!
    @IBOutlet weak var attemptsCircle: UIView!
    @IBOutlet weak var successCircle: UIView!

    let SELECTED_COLOR: UIColor = UIColor(red: 83 / 255.0, green: 59 / 255.0, blue: 83 / 255.0, alpha: 1)

    var selectedTicket: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        topBar.setTitle("Выберите билет")
        topBar.setInfoButtonImage("i-red", active: nil)
        topBar.backHandler = {
            self.navigationController?.popViewControllerAnimated(true)
        }
        topBar.infoHandler = {
            let popup = PopupView(frame: self.view.bounds, singleButton: true)

            popup.configure("Проверьте, готовы ли вы к экзамену",
                    desc: "У вас есть 20 минут, чтобы решить 20 вопросов. Вы можете сделать не более 2 ошибок.\n\nЕсли вы допустили ошибку, к билету добавляются 5 вопросов на тему, в которой вы ошиблись. В дополнительных вопросах ошибаться нельзя.",
                    icon: nil)

            popup.show({
                popup.hide()
            }, {})
        }
        topBar.backgroundColor = ToolbarChooseTicketBackgroundColor

        goToExam.backgroundColor = PositiveButtonBackgroundColor
        goToExam.setTitleColor(PositiveButtonTextColor, forState: .Normal)
        goToExam.alpha = 0
        successCount.alpha = 0
        attemptsCount.alpha = 0
        successCountLabel.alpha = 0
        attemptsCountLabel.alpha = 0
        successCircle.alpha = 0
        attemptsCircle.alpha = 0

        for i in 1...40 {
            let v = self.view.viewWithTag(i)
            if (v == nil) {
                continue
            }

            let b = v as! UIButton
            b.addTarget(self, action: #selector(ticketSelected), forControlEvents: UIControlEvents.TouchUpInside)
        }
    }

    func updateUIForSelectedTicket() {
        let appState = AppStateManager.instance.appState

        for i in 1...40 {
            let v = self.view.viewWithTag(i)
            if (v == nil) {
                continue
            }

            if (i == selectedTicket) {
                v!.backgroundColor = SELECTED_COLOR
                continue
            }

            let examInfo = appState.examInfo(i)
            if examInfo == nil {
                v!.backgroundColor = UIColor.clearColor()
                continue
            }

            if examInfo!.success() {
                v!.backgroundColor = ExamSuccessColor
            }
            else if examInfo!.failed() {
                v!.backgroundColor = ExamFailedColor
            }
            else {
                v!.backgroundColor = UIColor.clearColor()
            }
        }

        if selectedTicket > 0 {
            let examInfo = appState.examInfo(selectedTicket)

            let attempts = examInfo != nil ?examInfo!.attempts : 0
            let success = examInfo != nil ? examInfo!.successCount : 0

            attemptsCount.text = "\(attempts)"
            successCount.text = "\(success)"
        }
    }

    @IBAction func ticketSelected(sender: UIButton) {
        goToExam.alpha = 1
        successCount.alpha = 1
        attemptsCount.alpha = 1
        successCountLabel.alpha = 1
        attemptsCountLabel.alpha = 1
        successCircle.alpha = 1
        attemptsCircle.alpha = 1

        selectedTicket = sender.tag

        self.updateUIForSelectedTicket()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.navigationBarHidden = true
        updateUIForSelectedTicket()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

//        self.navigationController?.navigationBarHidden = false
    }

    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if selectedTicket > Prefs.freeTicketsCount && !Prefs.fullVersionBought {
            PopupView.showBuyPopup(self.view, viewController: self, source: "ChooseTicket")

            return false
        }

        return super.shouldPerformSegueWithIdentifier(identifier, sender: sender)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        let vc = segue.destinationViewController as! QuestionVC

        vc.isExam = true
        vc.ticketNumber = selectedTicket
        vc.topBarColor = topBar.backgroundColor

        AppStateManager.instance.appState.startAttempt(selectedTicket)
        Log.examStart(selectedTicket)
    }
}
