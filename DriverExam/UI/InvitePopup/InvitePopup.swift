//
// Created by alexey.nikitin on 14.05.16.
// Copyright (c) 2016 BadEnough. All rights reserved.
//

import Foundation
import Branch
import MJSnackBar

class InvitePopup: UIKit.UIView {
    @IBOutlet weak var closeButton: UIButton?
    @IBOutlet weak var sendButton: UIButton?
    @IBOutlet weak var ticketsPercents: ProgressLineView?
    @IBOutlet weak var questionsPercents: ProgressLineView?
    @IBOutlet weak var ticketsPercentsText: UILabel?
    @IBOutlet weak var questionsPercentsText: UILabel?
    @IBOutlet weak var urlLabel: UILabel?
    var viewController: UIViewController?
    var branchUniversalObject: BranchUniversalObject?
    var place: String?

    init(frame: CGRect, viewController: UIViewController) {
        super.init(frame: frame)

        self.viewController = viewController

        let view = NSBundle.mainBundle().loadNibNamed("InvitePopup", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.backgroundColor = PopupBackgroundColor

        view.frame = CGRectMake(5, 20, frame.size.width - 10, frame.size.height - 25)
        self.backgroundColor = UIColor(white: 0.1, alpha: 0.7)

        let freeQuestions = Prefs.freeQuestionsCountWithMark
        let freeQuestionsBranch = Prefs.branchQuestions
        let freeTickets = Prefs.freeTicketsCount
        let freeTicketsBranch = Prefs.branchTickets

        self.ticketsPercents?.progressColor = UIColor(red: 252.0 / 255, green: 101.0 / 255, blue: 108.0 / 255, alpha: 1)
        self.ticketsPercents?.progressCColor = UIColor(red: 230.0 / 255, green: 155.0 / 255, blue: 99.0 / 255, alpha: 1)
        self.ticketsPercents?.percents = min(100, 100 * freeTickets / 40)
        self.ticketsPercents?.percentsCenter = 100 * (freeTickets - freeTicketsBranch) / 40
        self.ticketsPercentsText?.text = "\(freeTickets) / 40"

        self.questionsPercents?.progressColor = UIColor(red: 41.0 / 255, green: 200.0 / 255, blue: 136.0 / 255, alpha: 1)
        self.questionsPercents?.progressCColor = UIColor(red: 20.0 / 255, green: 250.0 / 255, blue: 125.0 / 255, alpha: 1)
        self.questionsPercents?.percents = min(100 * freeQuestions / 800, 100)
        self.questionsPercents?.percentsCenter = min(100 * (freeQuestions - freeQuestionsBranch) / 800, 100)
        self.questionsPercentsText?.text = "\(freeQuestions) / 800"

/*
        self.closeButton?.backgroundColor = view.backgroundColor
        self.closeButton?.setTitleColor(PositiveButtonTextColor, forState: .Normal)
*/

        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"

        branchUniversalObject = BranchUniversalObject(canonicalIdentifier: "AddQuestions")
        branchUniversalObject?.title = "Отправь код"
        branchUniversalObject?.contentDescription = "И получи бонус!"
        self.urlLabel?.text = Prefs.lastInviteUrl
        branchUniversalObject?.getShortUrlWithLinkProperties(linkProperties, andCallback:{
            (url, error) in

            if error != nil {
                return
            }

            Prefs.lastInviteUrl = url
            self.urlLabel?.text = url
        })
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func show(place: String) {
        self.viewController?.view.addSubview(self);
        self.alpha = 0
        UIView.animateWithDuration(0.2, animations: {
            self.alpha = 1
        })

        Log.showInvitePopup(place)
        self.place = place
    }

    @IBAction func closeClicked() {
        self.removeFromSuperview()
    }

    @IBAction func sendCodeClicked() {
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        linkProperties.addControlParam("free_questions", withValue: String(Prefs.freeQuestionsCountInitial))
        linkProperties.addControlParam("free_tickets", withValue: String(Prefs.freeTicketsCountInitial))

        branchUniversalObject?.showShareSheetWithLinkProperties(linkProperties,
                andShareText: "Super amazing thing I want to share!",
                fromViewController: self.viewController,
                completion: { (activityType, completed) in
                    NSLog("done showing share sheet! \(activityType)")
                    Log.shareInviteLink(self.place)
                })
    }

    @IBAction func copyClicked() {
        let pasteboard = UIPasteboard.generalPasteboard();
        pasteboard.string = urlLabel?.text;

        var customSnackBar = Dictionary<String, Any>()
        customSnackBar["actionButtonText"] = ""
        customSnackBar["appearanceDuration"] = 1.2
        customSnackBar["animationTime"] = 0.12

        let sb = MJSnackBar(custom: customSnackBar)
        sb.show(self, message: "Ссылка скопирована в буфер обмена", completion: {reason in
        })

        Log.copyInviteLink(place)
    }
}
