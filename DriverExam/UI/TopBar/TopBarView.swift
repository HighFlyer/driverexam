//
// Created by Alexey Nikitin on 31.01.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit

class TopBarView: UIKit.UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    internal var backHandler: (() -> ())!
    internal var infoHandler: (() -> ())!

    override init(frame: CGRect) {
        super.init(frame: frame)

        let view = NSBundle.mainBundle().loadNibNamed("TopBar", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        let view = NSBundle.mainBundle().loadNibNamed("TopBar", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height)
    }

    func setTitle(title: String?) {
        titleLabel.text = title
    }

    func setInfoButtonImage(image: String, active: String?) {
        infoButton.setImage(UIImage(named: image), forState: .Normal)
        if active != nil {
            infoButton.setImage(UIImage(named: active!), forState: .Selected)
        }
    }

    @IBAction func backClicked(sender: AnyObject) {
        self.backHandler()
    }

    @IBAction func infoClicked(sender: AnyObject) {
        self.infoHandler()
    }
}
