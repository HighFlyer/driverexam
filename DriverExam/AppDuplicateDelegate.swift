//
//  AppDuplicateDelegate.swift
//  DriverExam
//
//  Created by alexey.nikitin on 07.04.16.
//  Copyright © 2016 BadEnough. All rights reserved.
//

import Foundation

@UIApplicationMain class AppDuplicateDelegate: AppBaseDelegate {
    private var signal: OneSignal!
    
    override func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        super.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        initFlurry()
        initOneSignal(launchOptions)
        initAppsFlyer()
        initYandex()
        
        return true
    }
    
    static func oneSignal() -> OneSignal! {
        let delegate = UIApplication.sharedApplication().delegate as! AppDuplicateDelegate
        return delegate.signal
    }
    
    func initFlurry() {
        Flurry.startSession("D48NTC6K79G32W86NFBP")
    }
    
    func initOneSignal(launchOptions: [NSObject: AnyObject]?) {
        signal = OneSignal(launchOptions: launchOptions, appId: "77d1e607-f795-483b-8af0-31855bd5494e", handleNotification: {
            (message, additionalData, isActive) in
            NSLog("\(message), \(additionalData), \(isActive)")
        })
        OneSignal.defaultClient().enableInAppAlertNotification(true)
        
        if Prefs.firstLaunch {
            signal.sendTag("showedPurchaseFormFromTraining", value: "no")
            Prefs.firstLaunch = false;
        }
        
        let type = Prefs.fullVersionBought ? "paid" : "free"
        signal.sendTag("type", value: type)
    }
    
    func initAppsFlyer() {
        AppsFlyerTracker.sharedTracker().appsFlyerDevKey = "F5K4nWD7RA3uFUtf4JaNun"
        AppsFlyerTracker.sharedTracker().appleAppID = "670312489"
    }
    
    func initYandex() {
        YMMYandexMetrica.activateWithApiKey("9e1f200d-c8e4-4b78-8794-7567d6ffa080")
        YMMYandexMetrica.setLoggingEnabled(true)
        YMMYandexMetrica.setReportCrashesEnabled(true)
    }
}
