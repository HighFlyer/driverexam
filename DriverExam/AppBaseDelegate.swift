//
// Created by alexey.nikitin on 05.03.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class AppBaseDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var helper: ProductsHelper = ProductsHelper()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject:AnyObject]?) -> Bool {
        application.statusBarStyle = UIStatusBarStyle.LightContent
        AppsFlyerTracker.sharedTracker().appsFlyerDevKey = "wDNfGkLU63CirZfm39tVvf"

        Flurry.setCrashReportingEnabled(true)

        // XXX
/*
        let docDirectory: NSString = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
        let logPath = docDirectory.stringByAppendingPathComponent("Logs.txt")
        freopen(logPath.cStringUsingEncoding(NSASCIIStringEncoding)!, "a+", stderr)
*/

        return true
    }

    func printFonts() {
        for (_, fontFamilyName) in UIFont.familyNames().enumerate() {
            for name in UIFont.fontNamesForFamilyName(fontFamilyName) {
                NSLog("Family: %@    Font: %@", fontFamilyName, name);
            }
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        AppStateManager.instance.save()
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        NSLog("applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppsFlyerTracker.sharedTracker().trackAppLaunch();
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    static func productsHelper() -> ProductsHelper! {
        let delegate = UIApplication.sharedApplication().delegate as! AppBaseDelegate
        return delegate.helper
    }
}
