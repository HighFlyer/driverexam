//
// Created by Alexey Nikitin on 07.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

class Utils {
    static func performDelayed(seconds: CGFloat, _ block: () -> ()) {
        let dispatchTime: dispatch_time_t = dispatch_time(DISPATCH_TIME_NOW, Int64(seconds * CGFloat(NSEC_PER_SEC)))
        dispatch_after(dispatchTime, dispatch_get_main_queue(), block)
    }

    static func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }

        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }

        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0

        return (isReachable && !needsConnection)    }
}
