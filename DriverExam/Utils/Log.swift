//
// Created by Alexey Nikitin on 27.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

class Log {
    static var lastShowPlace = ""

    static func trainingQuestionAnswered(isRight: String, questionId: Int) {
        let appState = AppStateManager.instance.appState;

        self.logEvent("TrainingQuestionAnswered", params:
        ["IsRight": isRight,
         "QuestionID": questionTransform(questionId),
         "UniqueQuestionSequenceNumber": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
         "TotalQuestionSequenceNumber": roundQuestion(appState.totalTrainingQuestionsAnswered)])

        self.logYandex("TrainingQuestionAnswered", params:
            ["IsRight": isRight,
             "QuestionID": questionTransform(questionId),
             "UniqueQuestionSequenceNumber": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
             "TotalQuestionSequenceNumber": roundQuestion(appState.totalTrainingQuestionsAnswered),
             "FreeQuestionsCount": String(Prefs.freeQuestionsCount)])
    }

    static func examQuestionAnswered(isRight: String, ticketNumber: Int, questionId: Int, sequenceNumber: Int32) {
        self.logEvent("ExamQuestionAnswered", params:
        ["IsRight": isRight,
         "TicketNumber": ticketNumber,
         "QuestionID": questionTransform(questionId),
         "QuestionSequenceNumber": Int(sequenceNumber)])
    }

    static func questionTransform(questionId: Int) -> String {
        do {
            let resultSet = try DBManager.instance.db.executeQuery("SELECT ticket_number, question_number FROM question WHERE _id = ?", questionId)

            if resultSet.next() {
                let result = resultSet.stringForColumnIndex(0) + "_" + resultSet.stringForColumnIndex(1)
                resultSet.close()
                return result
            } else {
                resultSet.close()
                return String(questionId)
            }
        } catch {
            NSLog("Failed to init trainingQuestions")
        }

        return String(questionId)
    }

    static func trainingFinished() {
        let appState = AppStateManager.instance.appState;

        self.logEvent("TrainingIsFinished", params:
        ["DurationTime": roundTotalTime(appState.totalTrainingSeconds)])
    }

    static func examStart(ticketNumber: Int) {
        self.logEvent("ExamStart", params: ["TicketNumber": ticketNumber])
    }

    static func examPassed(ticketNumber: Int, result: String) {
        self.logEvent("ExamPassed", params: ["TicketNumber": ticketNumber, "Result": result])
    }

    static func purchased(price: Int32) {
        let appState = AppStateManager.instance.appState;

        logEvent("PurchaseTier2", params:
        ["NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
         "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
         "NumberOfExamsPassed": appState.examsPassed()])

        logEvent("zPurchase_\(Prefs.freeQuestionsCount)_\(Prefs.priceID)", params: [
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered)
        ])

        logYandex("Purchase", params: [
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed(),
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "Price": String(price),
                "ExperimentNumberFreeQuestions": String(Prefs.questionsExperimentNumber),
                "ExperimentNumberPrice": String(Prefs.priceExperimentNumber),
                "PurchaseFormShowedAt": lastShowPlace,
                "FixPriceFormVariant": String(Prefs.priceFormVariant)
        ])
    }

    static func repurchased(price: Int32) {
        let appState = AppStateManager.instance.appState;

        logYandex("RePurchase", params: [
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed(),
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "Price": String(price),
                "ExperimentNumberFreeQuestions": String(Prefs.questionsExperimentNumber),
                "ExperimentNumberPrice": String(Prefs.priceExperimentNumber),
                "PurchaseFormShowedAt": lastShowPlace,
                "FixPriceFormVariant": String(Prefs.priceFormVariant)
        ])
    }

    static func ratePopupClosed(action: String) {
        logEvent("RateToGetQuestionsFromTraining", params: [
            "Rated": action
        ])

        logYandex("RateToGetQuestionsFromTraining", params: [
                "Rated": action,
                "NumberOfQuestions": String(FreeQuestionsForMark)
        ])
    }

    static func mainMenuClick(action: String!) {
        logEvent("GlobalMenuFormItemClicked", params:[
            "ItemName": action
        ])
    }

    static func purshaseFormClicked(place: String, _ action: String) {
        logEvent("PurchaseFormCliked", params:[
            "ItemName": action
        ])

        logYandex("PurchaseForm\(place)Cliked", params:[
            "ItemName": action,
            "FixPriceFormVariant": String(Prefs.priceFormVariant)
        ])
    }

    static func showBuyForm(place: String) {
        lastShowPlace = place;

        logEvent("PurchaseFormShowedUp", params: [
            "NumberOfTimes": Prefs.buyFormShowCount,
            "CalledFrom": place
        ])

        logEvent("zPurchaseFormShowedUp_\(Prefs.freeQuestionsCount)_\(Prefs.priceID)", params: [
            "NumberOfTimes": Prefs.buyFormShowCount,
            "CalledFrom": place
        ])

        logYandex("PurchaseFormShowedUp", params: [
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "ExperimentNumberFreeQuestions": String(Prefs.questionsExperimentNumber),
                "ExperimentNumberPrice": String(Prefs.priceExperimentNumber),
                "CalledFrom": place,
                "FixPriceFormVariant": String(Prefs.priceFormVariant)
        ])
    }

    static func showInvitePopup(place: String) {
        lastShowPlace = place
        let appState = AppStateManager.instance.appState;

        logYandex("RecommendationFormOpen", params: [
                "CalledFrom": place,
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed()
        ])
    }

    static func shareInviteLink(place: String?) {
        let appState = AppStateManager.instance.appState;

        logYandex("ShareRecommendationLink", params: [
                "CalledFrom": place != nil ? place! : "",
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed()
        ])
    }

    static func copyInviteLink(place: String?) {
        let appState = AppStateManager.instance.appState;

        logYandex("CopyRecommendationLink", params: [
                "CalledFrom": place != nil ? place! : "",
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed()
        ])
    }

    static func userGetAdditionalQuestions() {
        let appState = AppStateManager.instance.appState;

        logYandex("UserGetAdditionalQuestions", params: [
                "FreeQuestionsCount": String(Prefs.freeQuestionsCount),
                "PriceID": Prefs.priceID,
                "NumberOfTotalQuestionsAnswered": roundQuestion(appState.totalTrainingQuestionsAnswered),
                "NumberOfUniqueQuestionsAnswered": roundQuestion(appState.totalUniqueTrainingQuestionsAnswered),
                "NumberOfExamsPassed": appState.examsPassed()
        ])
    }

    static func purchaseRecovered(priceId: String!) {
        logYandex("PurchaseRecovery", params: [
            "PriceId": priceId
        ])
    }

    private static func roundQuestion(number: Int32) -> Int {
        return number - number % 10
    }

    private static func roundTotalTime(seconds: Int32) -> Int {
        let minutes = seconds / 60
        return minutes - minutes % 10
    }

    private static func logEvent(name: String, params: Dictionary<String, AnyObject>?) {
        if params != nil {
            Flurry.logEvent(name, withParameters: params)
        } else {
            Flurry.logEvent(name)
        }
    }

    private static func logYandex(name: String, params: Dictionary<String, AnyObject>?) {
        NSLog("event \(name) [\(params)]")
#if !PAID_VERSION
        let handler = {
            (error: NSError!) in

            NSLog("Failed to send event \(error)")
        }

        if params != nil {
            YMMYandexMetrica.reportEvent(name, parameters: params, onFailure: handler)
        }
        else {
            YMMYandexMetrica.reportEvent(name, onFailure: handler)
        }
#endif
    }
}
