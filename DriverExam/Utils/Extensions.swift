//
// Created by Alexey Nikitin on 03.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

extension Int {
    func format(f: Int) -> String {
        return String(format: "%0\(f)d", self)
    }
}

extension Int32 {
    func format(f: Int) -> String {
        return String(format: "%0\(f)d", self)
    }
}
