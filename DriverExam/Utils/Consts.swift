//
// Created by Alexey Nikitin on 23.02.16.
// Copyright (c) 2016 AppFlow. All rights reserved.
//

import Foundation

public let FreeQuestionsDefault = Int(200) // 200
public let FreeTicketsDefault = Int(20)
public let FreeQuestionsForMark = Int(100) // 100
public let ExamTimeout = 20 * 60
